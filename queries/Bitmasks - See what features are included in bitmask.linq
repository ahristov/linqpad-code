<Query Kind="Statements" />

long featuresMask = 4112;
var features = new Dictionary<byte, string>()
{
	{1, "Subscription"},
	{9, "Message read alerts"},
	{12, "Match phone"},
	{28, "Reply for free"},
	{29, "Private mode"},
};
foreach (var feature in features)
{
	bool included = ((long) Math.Pow(2, feature.Key - 1) & featuresMask) > 0;
	Console.WriteLine($"{feature.Value}({feature.Key}" + ") - {0}", included ? "is provisioned" : "is not provsioned");
}

