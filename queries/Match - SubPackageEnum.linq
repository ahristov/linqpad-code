<Query Kind="Program" />

void Main()
{
	foreach (var res in new[]
	{
		null as UserSubscriptionSummaryResult,
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 0 },
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 1 },
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 2 },
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 3 },
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 4 },
		new UserSubscriptionSummaryResult { SubscriptionPackageType = 5 },
	})
	{
		res.TemporaryFixGoldAndPlatinumAsPremium();
		Console.WriteLine(res);
	}
}


public class TemporaryFixGoldAndPlatinumAsPremiumTests
{
	
}


// Define other methods and classes here
public static class SubscriptionPackageTypeExtentions
{
	public static void TemporaryFixGoldAndPlatinumAsPremium(
		this IHasSubscriptionPackageType subPackageData)
	{
		if (subPackageData != null)
		{
			if (subPackageData.SubscriptionPackageType == (int)SubscriptionPackageTypeEnum.LatamGoldPackage
				|| subPackageData.SubscriptionPackageType == (int)SubscriptionPackageTypeEnum.LatamPlatinumPackage)
			{
				subPackageData.SubscriptionPackageType = (int)SubscriptionPackageTypeEnum.PremiumPackage;
			}
		}
	}
}

public interface IHasSubscriptionPackageType
{
	byte SubscriptionPackageType { get; set; }
}

public enum SubscriptionPackageTypeEnum : byte
{
	NonSubscriber = 0,
	StandardPackage = 1,
	PremiumPackage = 2,
	ElitePackage = 3,
	LatamGoldPackage = 4,
	LatamPlatinumPackage = 5,
}

public class UserSubscriptionSummaryResult : IHasSubscriptionPackageType
{
	public byte SubscriptionPackageType { get; set; }
	public string SubscriptionPackageTypeName => ((SubscriptionPackageTypeEnum)SubscriptionPackageType).ToString();
}