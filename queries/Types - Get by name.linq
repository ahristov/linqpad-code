<Query Kind="Program" />

void Main()
{
	//Console.WriteLine(Type.GetType("System.string", false, true).Name);
	//Console.WriteLine(Type.GetType("System.Int32").Name);
	//Console.WriteLine(Type.GetType("System.Decimal").Name);
	//Console.WriteLine(Type.GetType("System.DateTime").Name);


	Func<System.Reflection.AssemblyName, System.Reflection.Assembly> assemblyResolver = (aName) => {
		var res = aName.Name.Equals("System", StringComparison.InvariantCultureIgnoreCase)
			? Assembly.GetAssembly(Type.GetType("System.String"))
			: null;
		return res;
	};

	Func<System.Reflection.Assembly, string, bool, Type> typeResolver = (assem, name, ignore) =>
	{
		assem = typeof(System.String).Assembly;
		var res = assem.GetType(name, false, ignore);
		return res;
	};


	Console.WriteLine(Type.GetType("string", assemblyResolver, typeResolver, true, true));
}
