<Query Kind="Program" />

void Main()
{

}

// Define other methods, classes and namespaces here


[AttributeUsage(AttributeTargets.All)]
public class DescriptionAttribute : Attribute
{
	public string Description { get; private set; }
	
	public DescriptionAttribute(string description)
	{
		Description = description;
	}

	public override bool Equals(object obj)
	{
		if (obj == this)
			return true;

		var that = obj as DescriptionAttribute;
		if (that == null)
			return false;
			
		return string.Equals(that.Description, this.Description);
	}

	public override int GetHashCode() => (this.Description == null) ? base.GetHashCode() : this.Description.GetHashCode();
	public override bool IsDefaultAttribute() => string.IsNullOrWhiteSpace(this.Description);
}

public enum PaymentStatusEnum
{
	
}
