<Query Kind="Program">
  <NuGetReference>Hashids.net</NuGetReference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

void Main()
{
	int userId = 1892354;
	int rateCardKey = 101202;
	var promoId = "605";
	var product = new ExtendedProduct
	{
		ProductId = 80123,
		ParentEntityId = "parent",
		ISOCurrencyCode = "USD",
		BundleInformation = new BundleInformation
		{
			BundleId = 1,
			BundleMask = 256,
			BundleType = (byte)BundleType.Premium
		},
	};

	var token = ExtendedProductEntityTokenGeneratorV2.Encode(userId, rateCardKey, promoId, product);
	Console.WriteLine(token);

	ExtendedProductEntityTokenV2 tokenData;

	for (int i = userId; i <= userId + 10; i++)
	{
		if (!ExtendedProductEntityTokenGeneratorV2.TryDecode(i, token, out tokenData))
		{
			Console.WriteLine($"Error decoding product token for user {i}");
		}
		else
		{
			Console.WriteLine($"Success decoding product token for user {i}");
			Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(tokenData, Newtonsoft.Json.Formatting.Indented));
		}
	}

}

// Define other methods and classes here


[Serializable]
public class ExtendedProductEntityTokenV2
{
	public string Prefix { get; set; }
	public int RateCardKey { get; set; }
	public string PromoId { get; set; }
	public int ProductId { get; set; }
	public bool HasParent { get; set; }
	public string ISOCurrencyCode { get; set; }
	public byte BundleType { get; set; }
	public int? BundleId { get; set; }
	public long BundleMask { get; set; }
	public long Timestamp { get; set; }
	public string Hash { get; set; }
}


public static class ExtendedProductEntityTokenGeneratorV2
{
	static readonly HashidsNet.Hashids _hashIds = new HashidsNet.Hashids("Billing");
	
	const string PREFIX = "V2";
	const int PARTS = 11;
	const char _ = ':';
	const string TRUE = "1";
	const string FALSE = "";
	static readonly DateTime BEGIN_DT = new System.DateTime(2020, 5, 1);
	const long MINUTES_VALID = 1;

	public static string Encode(int userId, int rateCardKey, string promoId, ExtendedProduct product)
	{
		var tokenData = CreateInstance(rateCardKey, promoId, product);
		var res = TokenToString(userId, tokenData).TokenEncode();
		return res;
	}

	public static bool TryDecode(int userId, string token, out ExtendedProductEntityTokenV2 res)
	{
		res = null;

		if (string.IsNullOrWhiteSpace(token ?? ""))
			return false;

		token = token.TokenDecode();

		if (!token.StartsWith($"{PREFIX}{_}"))
			return false;

		var items = token.Split(_);
		if (items.Length != PARTS)
			return false;

		if (!VerifyToken(userId, items))
			return false;
		
		long ts = items[9].GetAsInt64();
		long now = (long)(DateTime.Now - BEGIN_DT).TotalMinutes;
		if (now - ts > MINUTES_VALID)
			return false;

		res = new ExtendedProductEntityTokenV2
		{
			Prefix = PREFIX,
			RateCardKey = items[1].GetAsInt(),
			PromoId = items[2],
			ProductId = items[3].GetAsInt(),
			HasParent = ("" + items[4]).Trim() == TRUE,
			ISOCurrencyCode = items[5],
			BundleType = items[6].GetAsByte(),
			BundleId = items[7].GetAsNullableInt(),
			BundleMask = items[8].GetAsInt64(),
			Timestamp = ts,
			Hash = items[10]
		};

		return true;
	}


	static ExtendedProductEntityTokenV2 CreateInstance(int rateCardKey, string promoId, ExtendedProduct product)
	{
		var res = new ExtendedProductEntityTokenV2
		{
			Prefix = PREFIX,
			RateCardKey = rateCardKey,
			PromoId = promoId,
			ProductId = product?.ProductId ?? 0,
			HasParent = string.IsNullOrWhiteSpace(product?.ParentEntityId ?? string.Empty) == false,
			ISOCurrencyCode = product?.ISOCurrencyCode,
			BundleType = product?.BundleInformation?.BundleType ?? 0,
			BundleId = product?.BundleInformation?.BundleId,
			BundleMask = product?.BundleInformation?.BundleMask ?? 0,
			Timestamp = (long)(DateTime.Now - BEGIN_DT).TotalMinutes,
		};

		return res;
	}

	static string TokenToString(int userId, ExtendedProductEntityTokenV2 t)
	{
		var data = string.Join(_, new object[]
		{
			t.Prefix, // 0
	        t.RateCardKey, // 1
	        t.PromoId, // 2
	        t.ProductId, // 3
	        t.HasParent ? TRUE : FALSE, // 4
	        t.ISOCurrencyCode, // 5
	        t.BundleType, // 6
	        t.BundleId, // 7
	        t.BundleMask, // 8
			t.Timestamp, // 9
		});
		
		var hash = _hashIds.Encode(userId, t.RateCardKey, t.ProductId, t.BundleType);
		return string.Join(_, new object[] { data, hash });
	}

	static bool VerifyToken(int userId, string[] items)
	{
		if (items.Length < 2)
			return false;
			
		string data = string.Join(_, items.Take(items.Length - 1).ToArray());
		var hash = _hashIds.Encode(userId, items[1].GetAsInt(), items[3].GetAsInt(), items[6].GetAsInt());
		var res = string.Equals(items[items.Length-1], hash);
		return res;
	}

	static string ComputeHash(string rawData)
	{  
		using (var sha = MD5.Create())
		{
			// ComputeHash - returns byte array  
			byte[] bytes = sha.ComputeHash(Encoding.UTF8.GetBytes(rawData));

			// Convert byte array to a string   
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < bytes.Length; i++)
			{
				builder.Append(bytes[i].ToString("x2"));
			}
			return builder.ToString();
		}
	}
}







// Products //////////////////////////////////////////////////////////////////////////////////////////////////////////

[Serializable]
public class ExtendedProduct : ICloneable
{
	public string EntityId { get; set; }
	public string ParentEntityId { get; set; }
	public int ProductId { get; set; }
	public short? FreeDays { get; set; }
	public DateTime BeginDt { get; set; }
	public short? InitialDays { get; set; }
	public short? RenewalDays { get; set; }
	public ProductPrice InitialPrice { get; set; }
	public ProductPrice RenewalPrice { get; set; }
	public ProductPrice InitialPricePerPeriod { get; set; }
	public ProductPrice RenewalPricePerPeriod { get; set; }
	public ProductPrice InitialPricePerUnit { get; set; }
	public ProductPrice RenewalPricePerUnit { get; set; }
	public string ISOCurrencyCode { get; set; }
	public decimal TaxRate { get; set; }
	public BundleInformation BundleInformation { get; set; }
	public IList<ProductSavingsData> ProductSavings { get; set; }
	public bool IsBestValue { get; set; }
	public bool IsGuarantee { get; set; }
	public bool IsBundled { get; set; }
	public bool ShowAddOnPage { get; set; }
	public bool IsFreeTrial { get; set; }
	public bool IsDelayCapture { get; set; }
	public string ExternalProductId { get; set; }
	public byte PrimaryFeature { get; set; }
	public string PrimaryFeatureName => ((ProductFeatureType)this.PrimaryFeature).ToString();
	public IList<ProductFeatureData> Features { get; set; }
	public DurationData Duration { get; set; }
	public DurationData RenewalDuration { get; set; }
	public QuantityData Quantity { get; set; }
	public IList<RateCardTypeData> CanPurchaseAs { get; set; }


	public ExtendedProduct()
		: base()
	{
		Features = new List<ProductFeatureData>();
		Duration = new DurationData();
		RenewalDuration = new DurationData();
		Quantity = new QuantityData();
		CanPurchaseAs = new List<RateCardTypeData>();
		ProductSavings = new List<ProductSavingsData>();
	}

	public ExtendedProduct Clone()
	{
		var res = this.MemberwiseClone() as ExtendedProduct;
		if (res == null)
			return res;

		res.EntityId = this.EntityId.CloneString();
		res.ParentEntityId = this.ParentEntityId.CloneString();
		res.InitialPrice = this.InitialPrice.CloneTyped();
		res.RenewalPrice = this.RenewalPrice.CloneTyped();
		res.InitialPricePerPeriod = this.InitialPricePerPeriod.CloneTyped();
		res.RenewalPricePerPeriod = this.RenewalPricePerPeriod.CloneTyped();
		res.InitialPricePerUnit = this.InitialPricePerUnit.CloneTyped();
		res.RenewalPricePerUnit = this.RenewalPricePerUnit.CloneTyped();
		res.ISOCurrencyCode = this.ISOCurrencyCode.CloneString();
		res.BundleInformation = this.BundleInformation.CloneTyped();
		res.ProductSavings = this.ProductSavings.CloneTypedCollection().ToList();
		res.Features = this.Features.CloneTypedCollection().ToList();
		res.Duration = this.Duration.CloneTyped();
		res.RenewalDuration = this.RenewalDuration.CloneTyped();
		res.Quantity = this.Quantity.CloneTyped();
		res.CanPurchaseAs = this.CanPurchaseAs.CloneTypedCollection().ToList();

		return res;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

[Serializable]
public class BundleInformation : ICloneable
{
	public byte BundleType { get; set; }
	public string BundleTypeName => ((BundleType)this.BundleType).ToString();

	public int? BundleId { get; set; }

	public long BundleMask { get; set; }

	public BundleInformation Clone()
	{
		return this.MemberwiseClone() as BundleInformation;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum BundleType : byte
{
	None = 0,
	Premium = 1,
	Upsell = 2,
	AddOn = 3,
	Elite = 4,
}


[Serializable]
public class ProductPrice : ICloneable
{
	public decimal PreDiscountedPrice { get; set; }
	public decimal DiscountPercentage { get; set; }
	public decimal DiscountAmount { get; set; }

	public decimal SalesPrice { get; set; }

	public decimal TaxRate { get; set; }
	public decimal TaxAmount { get; set; }
	public decimal TotalCost { get; set; }

	public ProductPrice Clone()
	{
		return this.MemberwiseClone() as ProductPrice;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}


[Serializable]
public class ProductFeatureData : ICloneable
{
	public byte ProductFeature { get; set; }
	public string ProductFeatureName => ((ProductFeatureType)ProductFeature).ToString();

	public ProductFeatureData()
	{ }

	public ProductFeatureData(byte featureType)
	{
		ProductFeature = featureType;
	}

	public ProductFeatureData(ProductFeatureType featureType)
	{
		ProductFeature = (byte)featureType;
	}

	public ProductFeatureData Clone()
	{
		return this.MemberwiseClone() as ProductFeatureData;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum ProductFeatureType : byte
{
	Undefined = 0,
	EmailCommunication = 1,
	ActiveProfile = 2,
	HighlightedProfile = 3,
	DateCoach = 4,
	FirstImpressions = 5,
	Voice = 6,
	PayForResponse = 7,
	MindFindBind = 8,
	EmailReadNotification = 9,
	MindFindBindSavings = 10,
	PlatinumServicesSavings = 11,
	MatchPhone = 12,
	MatchMobile = 13,
	SugarDaddyEmailToken = 14,
	RemoteApplicationEmailToken = 15,
	ProfileConsultingStandAloneProduct = 16,
	ProfileConsultingSubscriptionRequired = 17,
	Platinum = 18,
	PlatinumPackageSavings = 19,
	ChemistrySubscription = 20,
	MatchEventMemberTickets = 21,
	MatchEventGuestTicketsMale = 22,
	MatchEventGuestTicketsFemale = 23,
	ActivationFee = 24,
	PremiumBundleSavings = 25,
	TopSpot = 26,
	Stealth = 27,
	ReplyForFree = 28,
	PrivateMode = 29,
	MatchMe = 30,
	LoveToken = 31,
	SpeedVerify = 32,
	Intuition = 33,
	Invisibility = 34,
	Magnetism = 35,
	SuperStrength = 36,
	MatchIQ = 37,
	MatchVerification = 38,
	SuperLikes = 39,
}


[Serializable]
public class DurationData : ICloneable
{
	public decimal Length { get; set; }
	public int DurationUnit { get; set; }
	public string DurationUnitName => ((DurationUnitType)DurationUnit).ToString();

	public DurationData()
	{ }

	public DurationData(Duration duration)
	{
		Length = duration.Length;
		DurationUnit = (int)duration.Unit;
	}
	public DurationData Clone()
	{
		return this.MemberwiseClone() as DurationData;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum DurationUnitType
{
	Undefined,
	Second,
	Minute,
	Hour,
	Day,
	Week,
	Month,
	Year,
}

public class Duration
{
	public decimal Length { get; set; }
	public DurationUnitType Unit { get; set; }
}


[Serializable]
public class ProductSavingsData : ICloneable
{
	public int SavingsType { get; set; }
	public string SavingsTypeName => ((ProductSavingsType)SavingsType).ToString();
	public decimal RegularPriceSingle { get; set; }
	public decimal RegularPriceTotal { get; set; }
	public decimal PercentageSavings { get; set; }
	public ProductSavingsData Clone()
	{
		return this.MemberwiseClone() as ProductSavingsData;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum ProductSavingsType
{
	Undefined,
	SavingsToNonDiscountedOneMonth,
	SavingsToSmallestUnits
}


[Serializable]
public class QuantityData : ICloneable
{
	public decimal Amount { get; set; }
	public int QuantityUnit { get; set; }
	public string QuantityUnitName => ((QuantityUnitType)QuantityUnit).ToString();

	public QuantityData()
	{ }

	public QuantityData(Quantity quantity)
	{
		Amount = quantity.Amount;
		QuantityUnit = (int)quantity.Unit;
	}

	public QuantityData Clone()
	{
		return this.MemberwiseClone() as QuantityData;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum QuantityUnitType
{
	Undefined,
	Subscription,
	Unit,
}

public class Quantity
{
	public decimal Amount { get; set; }
	public QuantityUnitType Unit { get; set; }
}

[Serializable]
public class RateCardTypeData : ICloneable
{
	public int RateCardType { get; set; }
	public string RateCardTypeName => ((RateCardType)RateCardType).ToString();

	public RateCardTypeData()
	{ }

	public RateCardTypeData(RateCardType rateCardType)
	{
		RateCardType = (int)rateCardType;
	}

	public RateCardTypeData Clone()
	{
		return this.MemberwiseClone() as RateCardTypeData;
	}

	object ICloneable.Clone()
	{
		return Clone();
	}
}

public enum RateCardType : int
{
	Undefined = 0,
	Standard = 1,
	Upgrade = 2,
	Upsell = 3,
	PowerUp = 4,
	Unit = 5,
	UnitRenewal = 6,
}




// Cloneable Extensions /////////////////////////////////////////////////////////////////////////////////////////////////////////

public static class CloneableExtensions
{
	public static T CloneTyped<T>(this T obj)
		where T : ICloneable
	{
		if (obj == null)
			return default(T);

		return (T)obj.Clone();
	}

	public static IEnumerable<T> CloneTypedCollection<T>(this IEnumerable<T> c)
		where T : ICloneable
	{
		if (c != null)
		{
			foreach (var item in c)
				yield return item.CloneTyped();
		}
	}

	public static string CloneString(this string s)
	{
		return (s == null)
			? s
			: new System.Text.StringBuilder(s).ToString();
	}
}


// String extensions ///////////////////////////////////////////////////////////////////////////////////////////////


public static class StringExtensions
{
	public static string Truncate(this string value, int maxLength)
	{
		if (string.IsNullOrEmpty(value)) return value;
		return value.Length <= maxLength ? value : value.Substring(0, maxLength);
	}

	public static Tuple<string, string> ToFirstAndLastName(this string fullName)
	{
		try
		{
			var parts = ("" + fullName).Split(' ').ToList();
			string firstName = parts.FirstOrDefault();
			string lastName = string.Join(" ", parts.Skip(1).ToArray());
			return new Tuple<string, string>(firstName, lastName);
		}
		catch
		{
			return new Tuple<string, string>(fullName, string.Empty);
		}
	}


	public static string TokenEncode(this string str)
	{
		var toEncode = Encoding.UTF8.GetBytes(str);
		return HttpUtility.UrlTokenEncode(toEncode);
	}

	public static string TokenDecode(this string str)
	{
		var bytes = HttpUtility.UrlTokenDecode(str);
		if (bytes == null) return string.Empty;
		var returnValue = Encoding.UTF8.GetString(bytes);
		return returnValue;
	}

	public static byte? GetAsNullableByte(this string str) => byte.TryParse(str, out byte res) ? res : (byte?)null;
	public static byte GetAsByte(this string str) => byte.TryParse(str, out byte res) ? res : (byte)0;
	public static int? GetAsNullableInt(this string str) => int.TryParse(str, out int res) ? res : (int?)null;
	public static int GetAsInt(this string str) => int.TryParse(str, out int res) ? res : 0;
	public static long? GetAsNullableInt64(this string str) => long.TryParse(str, out long res) ? res : (long?)null;
	public static long GetAsInt64(this string str) => long.TryParse(str, out long res) ? res : 0;
	public static bool? GetAsNullableBoolean(this string str) => bool.TryParse(str, out bool res) ? res : (bool?)null;
	public static bool GetAsBoolean(this string str) => bool.TryParse(str, out bool res) ? res : false;
	public static DateTime? GetAsNullableDateTime(this string str) => DateTime.TryParse(str, out DateTime res) ? res : (DateTime?)null;
	public static DateTime GetAsDateTime(this string str) => DateTime.TryParse(str, out DateTime res) ? res : DateTime.MinValue;
}


public static class HttpUtility
{
	public static string UrlTokenEncode(byte[] input)
	{
		if (input == null) throw new ArgumentNullException(nameof(input));

		if (input.Length < 1) return string.Empty;

		var text = Convert.ToBase64String(input).AsSpan();

		if (text == null) return null;

		var num = text.Length;

		while (num > 0 && text[num - 1] == '=')
		{
			num--;
		}

		var baseArray = new char[num + 1];
		var array = baseArray.AsSpan();
		array[num] = (char)(48 + text.Length - num);

		for (int i = 0; i < num; i++)
		{
			var c = text[i];

			switch (c)
			{
				case '+':
					array[i] = '-';
					break;
				case '/':
					array[i] = '_';
					break;
				default:
					array[i] = c;
					break;
			}
		}

		return new string(baseArray);
	}

	public static byte[] UrlTokenDecode(string input)
	{
		if (input == null) throw new ArgumentNullException(nameof(input));

		int length = input.Length;

		if (length < 1) return new byte[0];

		var s_input = input.AsSpan();
		var num = (int)(s_input[length - 1] - '0');

		if (num < 0 || num > 10) return null;

		var arrayLength = length - 1 + num;
		var baseArray = new char[arrayLength];
		var array = baseArray.AsSpan();

		for (int i = 0; i < length - 1; i++)
		{
			var c = s_input[i];

			switch (c)
			{
				case '-':
					array[i] = '+';
					break;
				case '_':
					array[i] = '/';
					break;
				default:
					array[i] = c;
					break;
			}
		}

		for (int j = length - 1; j < arrayLength; j++)
		{
			array[j] = '=';
		}

		return Convert.FromBase64CharArray(baseArray, 0, arrayLength);
	}
}