<Query Kind="Program">
  <NuGetReference>FluentAssertions</NuGetReference>
  <NuGetReference>NUnit</NuGetReference>
  <Namespace>FluentAssertions</Namespace>
</Query>

void Main()
{
    string pattern = @"^match\.com\/(21\.0[3..9]|21\.[1..9]|2[23456789])";
    
    var sut = new RegExpTestingModule();
    sut.Init(pattern);
    
    foreach (var testData in GetTestData())
    {
        var expected = testData.Item2;
        var actual = sut.Evaluate(testData.Item1);
        actual.Should().Be(expected);
    }
}

// Define other methods and classes here



static IEnumerable<Tuple<string, bool>> GetTestData()
{
    yield return new Tuple<string, bool>(null
        , false);
    yield return new Tuple<string, bool>(""
        , false);
    yield return new Tuple<string, bool>("match.com/20.11.05 Android/8.1.0 (Motorola Moto G (5); resolution 1080x1776)"
        , false);
    yield return new Tuple<string, bool>("match.com/21.02.00 Android/10 (Motorola REVVLRY; resolution 720x1344)"
        , false);
    yield return new Tuple<string, bool>("match.com/21.02.00 Android/11 (Google Pixel 3; resolution 1080x2028)"
        , false);
    yield return new Tuple<string, bool>("match.com/21.03.00 Android/8.1.0 (Motorola Moto G (5); resolution 1080x1776)"
        , true);
    yield return new Tuple<string, bool>("match.com/20.03.05 Android/8.1.0 (Motorola Moto G (5); resolution 1080x1776)"
        , false);
    yield return new Tuple<string, bool>("match.com/25.03.05 Android/8.1.0 (Motorola Moto G (5); resolution 1080x1776)"
        , true);
    yield return new Tuple<string, bool>("com.match.match.com/21.04.00 iOS/14.4 (iPhone10,4; D201AP)"
        , false);
    yield return new Tuple<string, bool>("com.match.match.com/21.04.00 iOS/14.4 (iPhone12,1; N104AP)"
        , false);
    yield return new Tuple<string, bool>("com.match.match.com/21.04.00 iOS/14.4 (iPhone9,4; D111AP)"
        , false);
    yield return new Tuple<string, bool>("Mozilla/5.0 (iPhone; CPU iPhone OS 14_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Mobile/15E148 Safari/604.1"
        , false);
    yield return new Tuple<string, bool>("Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-N950U) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/13.2 Chrome/83.0.4103.106 Mobile Safari/537.36"
        , false);
}

public delegate object DataProviderCallback();

public interface ITestModule
{
    void Init(string criteria);
    bool Evaluate(DataProviderCallback dataCallback);
}

public class RegExpTestingModule : ITestModule
{
    private string _pattern = "";

    public void Init(string pattern)
    {
        _pattern = pattern;
    }

    public bool Evaluate(DataProviderCallback dataCallback)
    {
        object data = dataCallback();
        string s = data as string;
        return Evaluate(s);
    }

    public bool Evaluate(string s)
    {
        s = (""+s);
        bool res = false;

        try
        {
            Regex regExp = new Regex(_pattern);
            res = regExp.IsMatch(s);
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.ToString());
            res = false;
        }
        
        return res;
    }
}
