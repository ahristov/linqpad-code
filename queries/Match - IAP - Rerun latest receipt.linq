<Query Kind="Program">
  <Namespace>System.Net</Namespace>
</Query>

void Main()
{
	//Trust all certificates
	System.Net.ServicePointManager.ServerCertificateValidationCallback =
		((sender, certificate, chain, sslPolicyErrors) => true);

	foreach (var userId in Users)
	{
		string receipt = SelectLatestReceipts(userId);

		Console.WriteLine($"Processing {userId} ...");
		string resp = ApiProvision(userId, receipt); 
		Console.WriteLine(resp);
	}
}

// Define other methods and classes here
private static int[] Users = new int[]
{
	72231435   , // Replay the receipt
	167135888  , // Replay the receipt
	181690491  , // Replay the receipt
	280208418  , // Replay the receipt
	290896162  , // Replay the receipt
	291130654  , // Replay the receipt
	303537979  , // Replay the receipt
	312943696  , // Replay the receipt
	316689239  , // Replay the receipt
	317602508  , // Replay the receipt
	319763977  , // Replay the receipt
	322305385  , // Replay the receipt
	326168684  , // Replay the receipt
	331000515  , // Replay the receipt
	332089415  , // Replay the receipt
	332463756  , // Replay the receipt
	332890834  , // Replay the receipt
	334619869  , // Replay the receipt
	334797856  , // Replay the receipt
	
	325198605  , // Comped User Replay the receipt
	334718750  , // Comped User Replay the receipt
};

const string CNN_STR = "Server=MARP03\\MAREPORT01;Initial Catalog=WorkDB;Integrated Security=true;";
const int TIMEOUT = 1000;
const string BILLAPI_HOME = "https://10.10.128.151/api/v1/";
const string POST = "POST";
const string API_VERIFYRECEIPT = "inapp/verifyReceipt/apple/purchaseFlow";
const string API_PROVISIONRECEIPT = "provision/inapp/apple/purchaseFlow";

private static string ApiProvision(int userId, string receipt)
{
	string json = $@"
		{{
		  ""userId"": {userId},
		  ""platformId"": 1,
		  ""sessionId"" : ""00000000-0000-0000-0000-000000000000"",
		  ""clientIp1"" : 127,
		  ""clientIp2"" : 0,
		  ""clientIp3"" : 0,
		  ""clientIp4"" : 1,
		  ""decryptedReceiptJsonString"" : null,
		  ""receipt"" :	""{receipt}""
		}}
	";

	var res = ApiCall(POST, API_PROVISIONRECEIPT, json);
	return res;
}
private static string ApiVerifyReceipt(int userId, string receipt)
{
	string json = $@"
		{{
			""userId"" : {userId},
			""platformId"": 1,
			""sessionId"" : ""00000000-0000-0000-0000-000000000000"",
			""excludeOldTransactions"": true,
			""receipt"": ""{receipt}""
		}}
	";

	var res = ApiCall(POST, API_VERIFYRECEIPT, json);
	return res;
}

private static string ApiCall(string method, string url, string json)
{
	WebRequest request = WebRequest.Create($"{BILLAPI_HOME}{url}");
	request.Method = method;
	request.ContentType = "application/json";

	if (!string.IsNullOrWhiteSpace(json))
	{
		byte[] byteArray = Encoding.UTF8.GetBytes(json);
		request.ContentLength = byteArray.Length;

		using (Stream dataStream = request.GetRequestStream())
		{
			dataStream.Write(byteArray, 0, byteArray.Length);
			dataStream.Close();
			dataStream.Dispose();
		}
	}

	WebResponse response;

	try
	{
		response = request.GetResponse();
	}
	catch (WebException ex)
	{
		return ex.Message;
	}
	HttpWebResponse webResponse = (HttpWebResponse)response;

	if (webResponse.StatusCode != HttpStatusCode.OK)
	{
		Console.WriteLine($"Error: {webResponse.StatusCode} {webResponse.StatusDescription}");
		return null;
	}

	using (Stream dataStream = response.GetResponseStream())
	{
		StreamReader reader = new StreamReader(dataStream);
		string res = reader.ReadToEnd();
		return res;
	}
}

private static void RunSql(string sql, Action<SqlCommand> action)
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = sql;
			cmd.CommandTimeout = TIMEOUT;
			cmd.CommandType = CommandType.Text;

			action(cmd);
			
			cmd.Dispose();
		}
		cnn.Close();
		cnn.Dispose();
	}
}

private static string SelectLatestReceipts(int userId)
{
	string sql = $@"
select top 1 pl.Receipt
from BillingData.Apple.ProcessorLog pl
where pl.UserID = {userId}
and pl.Receipt is not null
order by pl.ProcessorLogID desc
";
	string res = null;

	RunSql(sql, (SqlCommand cmd) =>
	{
		using (var rdr = cmd.ExecuteReader())
		{
			while (rdr.Read())
			{
				if (DBNull.Value.Equals(rdr[0]))
				{
					res = null;
				}

				res = rdr.GetString(0);
			}

			rdr.Close();
		}
	});
	
	return res;
}