<Query Kind="Program" />

void Main()
{
	int mask = new List<RateCardType> {
		RateCardType.R1,
		RateCardType.R2,
		RateCardType.R3,
		RateCardType.R4,
		RateCardType.R5,
		RateCardType.R6,
		RateCardType.R7,
		RateCardType.R8,
		RateCardType.R9,
		// RateCardType.R10,
		RateCardType.R11,
		RateCardType.R12,
		RateCardType.R13,
		RateCardType.R14,
		RateCardType.R15,
		RateCardType.R16,
		RateCardType.R17,
		RateCardType.R18,
		RateCardType.R19,
		RateCardType.R20,
		RateCardType.R21,
		RateCardType.R22,
		RateCardType.R23,
		RateCardType.R24,
		RateCardType.R25,
		RateCardType.R26,
		RateCardType.R27,
		RateCardType.R28,
		RateCardType.R29,
		RateCardType.R30,
	}.EnumToUniqueBitMask();

	Console.WriteLine($"{mask} : {(mask & 1024) > 0}");
}

// Define other methods and classes here

public enum RateCardType : int
{
	Undefined = 0,
	R1 = 1,
	R2 = 2,
	R3 = 3,
	R4 = 4,
	R5 = 5,
	R6 = 6,
	R7 = 7,
	R8 = 8,
	R9 = 9,
	R10 =10,
	R11 =11,
	R12 =12,
	R13 =13,
	R14 =14,
	R15 =15,
	R16 =16,
	R17 =17,
	R18 =18,
	R19 =19,
	R20 =20,
	R21 =21,
	R22 =22,
	R23 =23,
	R24 =24,
	R25 =25,
	R26 =26,
	R27 =27,
	R28 =28,
	R29 =29,
	R30 =30,
}


public static class EnumExtensions
{
	public static int EnumToUniqueBitMask<T>(this IEnumerable<T> values)
		where T : struct, IConvertible
	{
		if (!typeof(T).IsEnum)
		{
			throw new ArgumentException("T must be enumeration");
		}

		if (values == null)
		{
			return 0;
		}

		Dictionary<T, int> unique = new Dictionary<T, int>();

		foreach (var val in values)
		{
			int i = Convert.ToInt32(val);
			unique[val] = (int)Math.Pow(2, i);
		}

		var res = unique.Sum(kvp => kvp.Value);
		return res;
	}
}
