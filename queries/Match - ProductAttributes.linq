<Query Kind="Program" />

void Main()
{

    /*
        foreach (int productId in new int[] { 0, 18187, 18188})
        {
            Console.WriteLine(productId.GetProductData());
            Console.WriteLine(productId.GetProductAttributes());
            Console.WriteLine(productId.GetIsRushHour());
        }
    */

    var listOdTotalsByProductID = new List<TotalsByProductID>
    {
        new TotalsByProductID { ProdID=null, PaidCount=10, FreeCount=0, UnusedPaidCount=0, UnusedFreeCount=0 },
        new TotalsByProductID { ProdID=5185, PaidCount=0, FreeCount=1, UnusedPaidCount=0, UnusedFreeCount=0 },
        new TotalsByProductID { ProdID=18073, PaidCount=0, FreeCount=6, UnusedPaidCount=0, UnusedFreeCount=3 },
        new TotalsByProductID { ProdID=18189, PaidCount=1, FreeCount=11, UnusedPaidCount=0, UnusedFreeCount=2 },
    };
    
    var listOfTotalsByProductType = TransformToProductsByType(listOdTotalsByProductID);
    
    Console.WriteLine(listOdTotalsByProductID);
    Console.WriteLine(listOfTotalsByProductType);
}

public static IEnumerable<TotalsByProductType> TransformToProductsByType(IEnumerable<TotalsByProductID> listOfTotalsByProductID)
{
    Dictionary<ProductAttributeTypeEnum, TotalsByProductType> collected = new Dictionary<ProductAttributeTypeEnum, TotalsByProductType>();

    foreach (var totalsByProductID in listOfTotalsByProductID)
    {
        ProductAttributeTypeEnum productType = ProductAttributeTypeEnum.None;
        
        if (totalsByProductID.ProdID.HasValue)
        {
            var productData = totalsByProductID.ProdID.Value.GetProductData();
            var productAttributes = productData?.ProductAttributes;
            if (productAttributes != null && productAttributes.Count() > 0)
            {
                productType = productAttributes[0];
            }
        }

        if (!collected.ContainsKey(productType))
        {
            collected.Add(productType, new TotalsByProductType() { ProductType = productType });
        }
        
        collected[productType].PaidCount += totalsByProductID.PaidCount;
        collected[productType].FreeCount += totalsByProductID.FreeCount;
        collected[productType].UnusedPaidCount += totalsByProductID.UnusedPaidCount;
        collected[productType].UnusedFreeCount += totalsByProductID.UnusedFreeCount;
    }
    
    return collected.ToArray().OrderBy(x=> x.Key).Select(x => x.Value);
}




[Serializable]
public class TotalsByProductID
{
    public int? ProdID { get; set; }
    public int PaidCount { get; set; }
    public int FreeCount { get; set; }
    public int UnusedPaidCount { get; set; }
    public int UnusedFreeCount { get; set; }
}

[Serializable]
public class TotalsByProductType
{
    public ProductAttributeTypeEnum ProductType { get; set; }
    public int PaidCount { get; set; }
    public int FreeCount { get; set; }
    public int UnusedPaidCount { get; set; }
    public int UnusedFreeCount { get; set; }
}


public static class ProductDataExtensions
{
    public static ProductDataAttribute GetProductData(this int productId)
    {
        if (!Enum.IsDefined(typeof(ProductsEnum), productId))
            return null;
            
        ProductsEnum product = (ProductsEnum)productId;
        ProductDataAttribute productData = product.GetProductData();
        return productData;
    }

    public static ProductDataAttribute GetProductData(this ProductsEnum product)
    {
        FieldInfo fi = product.GetType().GetField(product.ToString());
        if (fi == null)
            return null;
        ProductDataAttribute[] attributes =
            (ProductDataAttribute[])fi.GetCustomAttributes(typeof(ProductDataAttribute), false);
        return (attributes != null && attributes.Length > 0)
            ? attributes[0]
            : null;
    }

    public static ProductAttributeTypeEnum[] GetProductAttributes(this int productId)
        => productId.GetProductData()?.ProductAttributes ?? new ProductAttributeTypeEnum[] {};
        
    public static ProductAttributeTypeEnum[] GetProductAttributes(this ProductsEnum product)
        => product.GetProductData()?.ProductAttributes ?? new ProductAttributeTypeEnum[] {};

    static readonly ProductAttributeTypeEnum[] RushHourAttributeIDs = new ProductAttributeTypeEnum[] { ProductAttributeTypeEnum.RushhourBoost, };
    
    public static bool GetIsRushHour(this int productId)
        => productId.GetProductAttributes().Any(x => RushHourAttributeIDs.Contains(x));

    public static bool GetIsRushHour(this ProductsEnum product)
        => product.GetProductAttributes().Any(x => RushHourAttributeIDs.Contains(x));

}

[AttributeUsage(AttributeTargets.All)]
public class CanSaleAsPowerUpAttribute : Attribute
{
    public override bool Equals(object obj) => obj == this || obj is CanSaleAsPowerUpAttribute;
    public override int GetHashCode() => base.GetHashCode();
    public override bool IsDefaultAttribute() => true;
}

[AttributeUsage(AttributeTargets.All)]
public class FeatureProductIdAttribute : Attribute
{
    public int ProdId { get; private set; }

    public FeatureProductIdAttribute(int prodId)
    {
        ProdId = prodId;
    }

    public override bool Equals(object obj)
    {
        if (obj == this)
            return true;

        FeatureProductIdAttribute other = obj as FeatureProductIdAttribute;

        return (other != null) && other.ProdId == this.ProdId;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override bool IsDefaultAttribute()
    {
        return this.ProdId == 0;
    }
}

[AttributeUsage(AttributeTargets.All)]
public class IsImpulseFeatureAttribute : Attribute
{
    public ImpulseFeatureType ImpulseFeature { get; private set; }

    public IsImpulseFeatureAttribute(ImpulseFeatureType impulseFeature)
    {
        ImpulseFeature = impulseFeature;
    }

    public override bool Equals(object obj)
    {
        if (obj == this)
            return true;

        IsImpulseFeatureAttribute other = obj as IsImpulseFeatureAttribute;

        return (other != null) && other.ImpulseFeature == this.ImpulseFeature;
    }
    public override int GetHashCode() => base.GetHashCode();
    public override bool IsDefaultAttribute() => ImpulseFeature == ImpulseFeatureType.Undefined;
}

public enum ImpulseFeatureType : byte
{
    Undefined = 0,
    DateCoach = 4,
    TopSpot = 26,
    Undercover = 27,
    SuperLikes = 39,
}

public enum ProductFeatureType : byte
{
    Undefined = 0,
    EmailCommunication = 1,
    ActiveProfile = 2,
    HighlightedProfile = 3,
    DateCoach = 4,
    FirstImpressions = 5,
    Voice = 6,
    PayForResponse = 7,
    MindFindBind = 8,
    [CanSaleAsPowerUp]
    EmailReadNotification = 9,
    MindFindBindSavings = 10,
    PlatinumServicesSavings = 11,
    [CanSaleAsPowerUp]
    MatchPhone = 12,
    MatchMobile = 13,
    SugarDaddyEmailToken = 14,
    RemoteApplicationEmailToken = 15,
    ProfileConsultingStandAloneProduct = 16,
    ProfileConsultingSubscriptionRequired = 17,
    Platinum = 18,
    PlatinumPackageSavings = 19,
    ChemistrySubscription = 20,
    [FeatureProductId(18070)]
    MatchEventMemberTickets = 21,
    [FeatureProductId(18071)]
    MatchEventGuestTicketsMale = 22,
    [FeatureProductId(18072)]
    MatchEventGuestTicketsFemale = 23,
    ActivationFee = 24,
    PremiumBundleSavings = 25,
    [CanSaleAsPowerUp]
    [IsImpulseFeature(ImpulseFeatureType.TopSpot)]
    TopSpot = 26,
    [CanSaleAsPowerUp]
    [IsImpulseFeature(ImpulseFeatureType.Undercover)]
    Stealth = 27,
    [CanSaleAsPowerUp]
    ReplyForFree = 28,
    [CanSaleAsPowerUp]
    PrivateMode = 29,
    MatchMe = 30,
    LoveToken = 31,
    SpeedVerify = 32,
    Intuition = 33,
    Invisibility = 34,
    Magnetism = 35,
    SuperStrength = 36,
    MatchIQ = 37,
    MatchVerification = 38,
    [CanSaleAsPowerUp]
    [IsImpulseFeature(ImpulseFeatureType.SuperLikes)]
    SuperLikes = 39,
}

[Serializable]
public enum ProductAttributeTypeEnum
{
    None = 0,
    LoyaltyPricing = 1,
    MonthGuaranteeProgram = 2,
    CsaFreeAddOnExistingSubs = 3,
    OneTimeEventServiceFeeDoNotDisplayOnRateCard = 4,
    UpsellProductDoNotDisplayOnRateCard = 5,
    Installments = 6,
    AppleIosInAppPurchase = 7,
    MonthlyInstallmentsCollectedByPaymentProcessor = 8,
    RushhourBoost = 9,
    BoostWithRff = 10,
}

    public class ProductDataAttribute: Attribute
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public ProductFeatureType ProductFeature { get; set; }
        public bool IsRenewable { get; set; }
        public short NumberOfUnits { get; set; }
        public short ImpulseMinutes { get; set; }
        public ProductAttributeTypeEnum[] ProductAttributes { get; set; }

        public ProductDataAttribute(
            int productID, 
            string productName,
            ProductFeatureType productFeature,
            bool isRenewable,
            short numberOfUnits,
            short impulseMinutes,
            ProductAttributeTypeEnum[] productAttributes)
        {
            ProductID = productID;
            ProductName = productName;
            ProductFeature = productFeature;
            IsRenewable = isRenewable;
            NumberOfUnits = numberOfUnits;
            ImpulseMinutes = impulseMinutes;
            ProductAttributes = productAttributes;
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            ProductDataAttribute other = obj as ProductDataAttribute;
            if (other == null
                || other.ProductID != this.ProductID
                || other.ProductName != this.ProductName
                || other.ProductFeature != this.ProductFeature
                || other.IsRenewable != this.IsRenewable
                || other.NumberOfUnits != this.NumberOfUnits
                || other.ImpulseMinutes != this.ImpulseMinutes)
                return false;

            var sum1 = GetBitMask(this.ProductAttributes ?? new ProductAttributeTypeEnum[] { });
            var sum2 = GetBitMask(other.ProductAttributes ?? new ProductAttributeTypeEnum[] { });

            return sum1 == sum2;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => this.ProductID == 0;

        private static long GetBitMask(ProductAttributeTypeEnum[] items)
        {
            var sum = items
                        .Where(x => (int)x > 0)
                        .Select(x => (int)x)
                        .Distinct()
                        .Sum(x => Math.Pow(2, x - 1));
        return (long)sum;
    }

}

    [Serializable]
    public enum ProductsEnum
    {
        [ProductData(5001, "1 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5001 = 5001,
        [ProductData(5002, "3 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5002 = 5002,
        [ProductData(5003, "6 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5003 = 5003,
        [ProductData(5004, "12 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5004 = 5004,
        [ProductData(5005, "3 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5005 = 5005,
        [ProductData(5006, "6 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5006 = 5006,
        [ProductData(5007, "12 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5007 = 5007,
        [ProductData(5008, "3 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5008 = 5008,
        [ProductData(5009, "5 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5009 = 5009,
        [ProductData(5010, "7 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5010 = 5010,
        [ProductData(5011, "10 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5011 = 5011,
        [ProductData(5012, "14 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5012 = 5012,
        [ProductData(5013, "21 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5013 = 5013,
        [ProductData(5014, "30 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5014 = 5014,
        [ProductData(5015, "60 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5015 = 5015,
        [ProductData(5016, "90 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5016 = 5016,
        [ProductData(5022, "1 Month Active Profile", (ProductFeatureType)2, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthActiveProfile_5022 = 5022,
        [ProductData(5024, "1 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5024 = 5024,
        [ProductData(5026, "1 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5026 = 5026,
        [ProductData(5030, "3 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5030 = 5030,
        [ProductData(5032, "3 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5032 = 5032,
        [ProductData(5036, "6 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5036 = 5036,
        [ProductData(5038, "6 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5038 = 5038,
        [ProductData(5039, "12 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5039 = 5039,
        [ProductData(5040, "12 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5040 = 5040,
        [ProductData(5041, "5 days for $5, one-month renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DaysForOneMonthRenewal_5041 = 5041,
        [ProductData(5042, "15 days for $15, one-month renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DaysForOneMonthRenewal_5042 = 5042,
        [ProductData(5043, "Loyalty Pricing", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)1 })]
        LoyaltyPricing_5043 = 5043,
        [ProductData(5044, "6 Month Base Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)2 })]
        MonthBaseSubscription_5044 = 5044,
        [ProductData(5045, "1 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5045 = 5045,
        [ProductData(5046, "3 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5046 = 5046,
        [ProductData(5047, "3 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5047 = 5047,
        [ProductData(5048, "6 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5048 = 5048,
        [ProductData(5049, "6 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5049 = 5049,
        [ProductData(5050, "12 Month MindFindBind", (ProductFeatureType)8, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbind_5050 = 5050,
        [ProductData(5051, "6 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)2 })]
        MonthBaseSubscription_5051 = 5051,
        [ProductData(5052, "6 Month Base Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)2 })]
        MonthBaseSubscription_5052 = 5052,
        [ProductData(5053, " 3 Month Highlighted Profile With 1 Month Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfileWithMonthRenewal_5053 = 5053,
        [ProductData(5054, " 6 Month Highlighted Profile With 1 Month Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfileWithMonthRenewal_5054 = 5054,
        [ProductData(5055, "12 Month Highlighted Profile With 1 Month Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfileWithMonthRenewal_5055 = 5055,
        [ProductData(5056, " 3 Month First Impressions With 1 Month Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressionsWithMonthRenewal_5056 = 5056,
        [ProductData(5057, " 6 Month First Impressions With 1 Month Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressionsWithMonthRenewal_5057 = 5057,
        [ProductData(5058, "12 Month First Impressions With 1 Month Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressionsWithMonthRenewal_5058 = 5058,
        [ProductData(5059, "6 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5059 = 5059,
        [ProductData(5060, "6 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5060 = 5060,
        [ProductData(5061, "6 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5061 = 5061,
        [ProductData(5062, "3 Day Email Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayEmailSubscription_5062 = 5062,
        [ProductData(5063, "3 Day Highlighted Profile", (ProductFeatureType)3, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayHighlightedProfile_5063 = 5063,
        [ProductData(5064, "3 Day First Impressions", (ProductFeatureType)5, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFirstImpressions_5064 = 5064,
        [ProductData(5065, "1 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_5065 = 5065,
        [ProductData(5066, "3 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_5066 = 5066,
        [ProductData(5067, "6 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_5067 = 5067,
        [ProductData(5068, "1 Month Platinum Service Savings", (ProductFeatureType)11, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinumServiceSavings_5068 = 5068,
        [ProductData(5069, "3 Month Platinum Service Savings", (ProductFeatureType)11, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinumServiceSavings_5069 = 5069,
        [ProductData(5070, "6 Month Platinum Service Savings", (ProductFeatureType)11, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinumServiceSavings_5070 = 5070,
        [ProductData(5071, "1 Month MindFindBind Savings", (ProductFeatureType)10, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbindSavings_5071 = 5071,
        [ProductData(5072, "3 Month MindFindBind Savings", (ProductFeatureType)10, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbindSavings_5072 = 5072,
        [ProductData(5073, "6 Month MindFindBind Savings", (ProductFeatureType)10, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMindfindbindSavings_5073 = 5073,
        [ProductData(5074, "1 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_5074 = 5074,
        [ProductData(5075, "3 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_5075 = 5075,
        [ProductData(5076, "6 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_5076 = 5076,
        [ProductData(5077, "CSA Free AddOn : 1 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfile_5077 = 5077,
        [ProductData(5078, "CSA Free AddOn : 3 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfile_5078 = 5078,
        [ProductData(5079, "CSA Free AddOn : 3 Month Highlighted Profile With 30-Day Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfileWithDayRenewal_5079 = 5079,
        [ProductData(5080, "CSA Free AddOn : 6 Month Highlighted Profile ", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfile_5080 = 5080,
        [ProductData(5081, "CSA Free AddOn : 6 Month Highlighted Profile With 30-Day Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfileWithDayRenewal_5081 = 5081,
        [ProductData(5082, "CSA Free AddOn : 12 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfile_5082 = 5082,
        [ProductData(5083, "CSA Free AddOn : 12 Month Highlighted Profile With 30-Day Renewal", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthHighlightedProfileWithDayRenewal_5083 = 5083,
        [ProductData(5084, "CSA Free AddOn : 1 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressions_5084 = 5084,
        [ProductData(5085, "CSA Free AddOn : 3 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressions_5085 = 5085,
        [ProductData(5086, "CSA Free AddOn : 3 Month First Impressions With 30-Day Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressionsWithDayRenewal_5086 = 5086,
        [ProductData(5087, "CSA Free AddOn : 6 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressions_5087 = 5087,
        [ProductData(5088, "CSA Free AddOn : 6 Month First Impressions With 30-Day Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressionsWithDayRenewal_5088 = 5088,
        [ProductData(5089, "CSA Free AddOn : 12 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressions_5089 = 5089,
        [ProductData(5090, "CSA Free AddOn : 12 Month First Impressions With 30-Day Renewal", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthFirstImpressionsWithDayRenewal_5090 = 5090,
        [ProductData(5091, "CSA Free AddOn : 1 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlerts_5091 = 5091,
        [ProductData(5092, "CSA Free AddOn : 3 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlerts_5092 = 5092,
        [ProductData(5093, "CSA Free AddOn : 3 Month Message Read Alerts With 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlertsWithDayRenewal_5093 = 5093,
        [ProductData(5094, "CSA Free AddOn : 6 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlerts_5094 = 5094,
        [ProductData(5095, "CSA Free AddOn : 6 Month Message Read Alerts With 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlertsWithDayRenewal_5095 = 5095,
        [ProductData(5096, "CSA Free AddOn : 12 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlerts_5096 = 5096,
        [ProductData(5097, "CSA Free AddOn : 12 Month Message Read Alerts With 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)3 })]
        CsaFreeAddonMonthMessageReadAlertsWithDayRenewal_5097 = 5097,
        [ProductData(5098, "1 Month matchMobile", (ProductFeatureType)13, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchmobile_5098 = 5098,
        [ProductData(5099, "3 Month matchMobile", (ProductFeatureType)13, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchmobile_5099 = 5099,
        [ProductData(5100, "6 Month matchMobile", (ProductFeatureType)13, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchmobile_5100 = 5100,
        [ProductData(5101, "Sugar Daddy (Per Unit)", (ProductFeatureType)14, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        SugarDaddyPerUnit_5101 = 5101,
        [ProductData(5102, "Sugar Daddy Five-Pack", (ProductFeatureType)14, false, 5, 0, new ProductAttributeTypeEnum[] {  })]
        SugarDaddyFivePack_5102 = 5102,
        [ProductData(5103, "Sugar Daddy Ten-Pack", (ProductFeatureType)14, false, 10, 0, new ProductAttributeTypeEnum[] {  })]
        SugarDaddyTenPack_5103 = 5103,
        [ProductData(5104, "RDA Email Token (Per Unit)", (ProductFeatureType)15, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        RdaEmailTokenPerUnit_5104 = 5104,
        [ProductData(5105, "RDA Email Token Five-Pack", (ProductFeatureType)15, false, 5, 0, new ProductAttributeTypeEnum[] {  })]
        RdaEmailTokenFivePack_5105 = 5105,
        [ProductData(5106, "RDA Email Token Ten-Pack", (ProductFeatureType)15, false, 10, 0, new ProductAttributeTypeEnum[] {  })]
        RdaEmailTokenTenPack_5106 = 5106,
        [ProductData(5107, "Profile Consulting (Stand-Alone Product)", (ProductFeatureType)16, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        ProfileConsultingStandAloneProduct_5107 = 5107,
        [ProductData(5108, "Profile Consulting (Subscription Required)", (ProductFeatureType)17, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        ProfileConsultingSubscriptionRequired_5108 = 5108,
        [ProductData(5109, "5 Day Email Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayEmailSubscription_5109 = 5109,
        [ProductData(5114, "3 Month Email for price of one month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthEmailForPriceOfOneMonth_5114 = 5114,
        [ProductData(5116, "1 Month Platinum", (ProductFeatureType)18, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinum_5116 = 5116,
        [ProductData(5117, "3 Month Platinum", (ProductFeatureType)18, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinum_5117 = 5117,
        [ProductData(5118, "6 Month Platinum", (ProductFeatureType)18, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinum_5118 = 5118,
        [ProductData(5119, "6 Month Platinum Discount", (ProductFeatureType)19, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPlatinumDiscount_5119 = 5119,
        [ProductData(5120, "3 Month Base Subscription (test)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionTest_5120 = 5120,
        [ProductData(5121, "1 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5121 = 5121,
        [ProductData(5122, "2 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5122 = 5122,
        [ProductData(5123, "4 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5123 = 5123,
        [ProductData(5124, "6 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5124 = 5124,
        [ProductData(5125, "8 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5125 = 5125,
        [ProductData(5126, "9 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5126 = 5126,
        [ProductData(5127, "11 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5127 = 5127,
        [ProductData(5128, "12 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5128 = 5128,
        [ProductData(5129, "13 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_5129 = 5129,
        [ProductData(5130, "1 Month Chemistry Subscription Add-on", (ProductFeatureType)20, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthChemistrySubscriptionAddOn_5130 = 5130,
        [ProductData(5131, "3 Month Chemistry Subscription Add-on", (ProductFeatureType)20, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthChemistrySubscriptionAddOn_5131 = 5131,
        [ProductData(5132, "6 Month Chemistry Subscription Add-on", (ProductFeatureType)20, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthChemistrySubscriptionAddOn_5132 = 5132,
        [ProductData(5133, "3 Day Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DaySubscription_5133 = 5133,
        [ProductData(5134, "3 Day Highlighted Profile", (ProductFeatureType)3, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayHighlightedProfile_5134 = 5134,
        [ProductData(5135, "3 Day First Impressions", (ProductFeatureType)5, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFirstImpressions_5135 = 5135,
        [ProductData(5136, "3 Day  Message Read Alerts ", (ProductFeatureType)9, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMessageReadAlerts_5136 = 5136,
        [ProductData(5137, "3 Day matchPhone", (ProductFeatureType)12, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMatchphone_5137 = 5137,
        [ProductData(5138, "7 Day Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DaySubscription_5138 = 5138,
        [ProductData(5139, "7 Day Highlighted Profile", (ProductFeatureType)3, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayHighlightedProfile_5139 = 5139,
        [ProductData(5140, "7 Day First Impressions", (ProductFeatureType)5, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFirstImpressions_5140 = 5140,
        [ProductData(5141, "7 Day  Message Read Alerts ", (ProductFeatureType)9, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMessageReadAlerts_5141 = 5141,
        [ProductData(5142, "7 Day matchPhone", (ProductFeatureType)12, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMatchphone_5142 = 5142,
        [ProductData(5143, "14 Day Subscription", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DaySubscription_5143 = 5143,
        [ProductData(5144, "14 Day Highlighted Profile", (ProductFeatureType)3, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayHighlightedProfile_5144 = 5144,
        [ProductData(5145, "14 Day First Impressions", (ProductFeatureType)5, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFirstImpressions_5145 = 5145,
        [ProductData(5146, "14 Day  Message Read Alerts ", (ProductFeatureType)9, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMessageReadAlerts_5146 = 5146,
        [ProductData(5147, "14 Day matchPhone", (ProductFeatureType)12, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayMatchphone_5147 = 5147,
        [ProductData(5154, "Match Event Member Tickets", (ProductFeatureType)21, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventMemberTickets_5154 = 5154,
        [ProductData(5155, "Match Event Guest Tickets - Male", (ProductFeatureType)22, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventGuestTicketsMale_5155 = 5155,
        [ProductData(5156, "Match Event Guest Tickets - Female", (ProductFeatureType)23, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventGuestTicketsFemale_5156 = 5156,
        [ProductData(5157, "Activation Fee", (ProductFeatureType)24, false, 1, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)4 })]
        ActivationFee_5157 = 5157,
        [ProductData(5158, "3 Month Bundle Upsell Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)5 })]
        MonthBundleUpsellSubscription_5158 = 5158,
        [ProductData(5159, "6 Month Bundle Upsell Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)2,(ProductAttributeTypeEnum)5 })]
        MonthBundleUpsellSubscription_5159 = 5159,
        [ProductData(5160, "12 Month Bundle Upsell Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)5 })]
        MonthBundleUpsellSubscription_5160 = 5160,
        [ProductData(5161, "12 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_5161 = 5161,
        [ProductData(5162, "1 Month Premium Bundle Savings", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPremiumBundleSavings_5162 = 5162,
        [ProductData(5163, "3 Month Premium Bundle Savings", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPremiumBundleSavings_5163 = 5163,
        [ProductData(5164, "6 Month Premium Bundle Savings", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPremiumBundleSavings_5164 = 5164,
        [ProductData(5165, "12 Month Matchphone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_5165 = 5165,
        [ProductData(5166, "12 Month Premium Bundle Savings", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPremiumBundleSavings_5166 = 5166,
        [ProductData(5167, "6 Month Installment Package", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthInstallmentPackage_5167 = 5167,
        [ProductData(5168, "6 Month Highlighted Profile - Installment", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthHighlightedProfileInstallment_5168 = 5168,
        [ProductData(5169, "6 Month First Impressions - Installment", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthFirstImpressionsInstallment_5169 = 5169,
        [ProductData(5170, "6 Month Message Read Alerts - Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthMessageReadAlertsInstallment_5170 = 5170,
        [ProductData(5171, "6 Month matchPhone - Installment", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthMatchphoneInstallment_5171 = 5171,
        [ProductData(5172, "12 Month Installment Package", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthInstallmentPackage_5172 = 5172,
        [ProductData(5173, "12 Month Highlighted Profile - Installment", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthHighlightedProfileInstallment_5173 = 5173,
        [ProductData(5174, "12 Month First Impressions - Installment", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthFirstImpressionsInstallment_5174 = 5174,
        [ProductData(5175, "12 Month Message Read Alerts - Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthMessageReadAlertsInstallment_5175 = 5175,
        [ProductData(5176, "12 Month matchPhone - Installment", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthMatchphoneInstallment_5176 = 5176,
        [ProductData(5177, "6 Month Premium Bundle Savings - Installment", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthPremiumBundleSavingsInstallment_5177 = 5177,
        [ProductData(5178, "12 Month Premium Bundle Savings - Installment", (ProductFeatureType)25, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)6 })]
        MonthPremiumBundleSavingsInstallment_5178 = 5178,
        [ProductData(5185, "TopSpot", (ProductFeatureType)26, false, 1, 60, new ProductAttributeTypeEnum[] {  })]
        Topspot_5185 = 5185,
        [ProductData(5186, "Stealth", (ProductFeatureType)27, false, 1, 1440, new ProductAttributeTypeEnum[] {  })]
        Stealth_5186 = 5186,
        [ProductData(5187, "TopSpot 15 Mins", (ProductFeatureType)26, false, 1, 15, new ProductAttributeTypeEnum[] {  })]
        TopspotMins_5187 = 5187,
        [ProductData(5188, "TopSpot 30 Mins", (ProductFeatureType)26, false, 1, 30, new ProductAttributeTypeEnum[] {  })]
        TopspotMins_5188 = 5188,
        [ProductData(5190, "6 Month Base Subscription (Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_5190 = 5190,
        [ProductData(5191, "6 Month Message Read Alerts (Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_5191 = 5191,
        [ProductData(5192, "3 Month Base Subscription (Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_5192 = 5192,
        [ProductData(5193, "3 Month Message Read Alerts (Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_5193 = 5193,
        [ProductData(5194, "1 Month Base Subscription (Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_5194 = 5194,
        [ProductData(5195, "1 Month Message Read Alerts (Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_5195 = 5195,
        [ProductData(5196, "Kiss 1 Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        KissMonth_5196 = 5196,
        [ProductData(5197, "Kiss 3 Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        KissMonth_5197 = 5197,
        [ProductData(5198, "Kiss 12 Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        KissMonth_5198 = 5198,
        [ProductData(5199, "Kiss 3 Month (Lifetime)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        KissMonthLifetime_5199 = 5199,
        [ProductData(5204, "TopSpot 5 Pack", (ProductFeatureType)26, false, 5, 60, new ProductAttributeTypeEnum[] {  })]
        TopspotPack_5204 = 5204,
        [ProductData(5205, "TopSpot 10 Pack", (ProductFeatureType)26, false, 10, 60, new ProductAttributeTypeEnum[] {  })]
        TopspotPack_5205 = 5205,
        [ProductData(5206, "1 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_5206 = 5206,
        [ProductData(5207, "3 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_5207 = 5207,
        [ProductData(5208, "6 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_5208 = 5208,
        [ProductData(5209, "12 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_5209 = 5209,
        [ProductData(5210, "MatchMe", (ProductFeatureType)30, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        Matchme_5210 = 5210,
        [ProductData(5211, "Stealth 60 Mins", (ProductFeatureType)27, false, 1, 60, new ProductAttributeTypeEnum[] {  })]
        StealthMins_5211 = 5211,
        [ProductData(5212, "Stealth 30 Mins", (ProductFeatureType)27, false, 1, 30, new ProductAttributeTypeEnum[] {  })]
        StealthMins_5212 = 5212,
        [ProductData(5213, "SpeedDate Paid 3-day trial", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddatePaidDayTrial_5213 = 5213,
        [ProductData(5214, "SpeedDate Paid 3-day trial", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddatePaidDayTrial_5214 = 5214,
        [ProductData(5215, "SpeedDate 1-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5215 = 5215,
        [ProductData(5216, "SpeedDate 3-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5216 = 5216,
        [ProductData(5217, "SpeedDate 6-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5217 = 5217,
        [ProductData(5218, "SpeedDate 1-Year", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateYear_5218 = 5218,
        [ProductData(5219, "SpeedDate 2-Year", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateYear_5219 = 5219,
        [ProductData(5220, "SpeedDate 3-Year", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateYear_5220 = 5220,
        [ProductData(5221, "SpeedDate 1-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5221 = 5221,
        [ProductData(5222, "SpeedDate 3-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5222 = 5222,
        [ProductData(5223, "SpeedDate 6-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateMonth_5223 = 5223,
        [ProductData(5224, "SpeedDate 1-Year", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateYear_5224 = 5224,
        [ProductData(5225, "1 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_5225 = 5225,
        [ProductData(5226, "3 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_5226 = 5226,
        [ProductData(5227, "6 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_5227 = 5227,
        [ProductData(5228, "12 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_5228 = 5228,
        [ProductData(5229, "6 Month Private Mode - Installment", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateModeInstallment_5229 = 5229,
        [ProductData(5230, "12 Month Private Mode - Installment", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateModeInstallment_5230 = 5230,
        [ProductData(5231, "7 Day Free Trial (Apple IAP)", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        DayFreeTrialAppleIap_5231 = 5231,
        [ProductData(5232, "20 Love Tokens", (ProductFeatureType)31, false, 20, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5232 = 5232,
        [ProductData(5233, "40 Love Tokens", (ProductFeatureType)31, false, 40, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5233 = 5233,
        [ProductData(5234, "60 Love Tokens", (ProductFeatureType)31, false, 60, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5234 = 5234,
        [ProductData(5235, "200 Love Tokens", (ProductFeatureType)31, false, 200, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5235 = 5235,
        [ProductData(5236, "255 Love Tokens", (ProductFeatureType)31, false, 255, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5236 = 5236,
        [ProductData(5237, "150 Love Tokens", (ProductFeatureType)31, false, 150, 0, new ProductAttributeTypeEnum[] {  })]
        LoveTokens_5237 = 5237,
        [ProductData(5238, "Speed Verify", (ProductFeatureType)32, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        SpeedVerify_5238 = 5238,
        [ProductData(5239, "SpeedDate Female 1-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateFemaleMonth_5239 = 5239,
        [ProductData(5240, "SpeedDate Female 3-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateFemaleMonth_5240 = 5240,
        [ProductData(5241, "SpeedDate Female 6-Month", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateFemaleMonth_5241 = 5241,
        [ProductData(5242, "SpeedDate Female 1-Year", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        SpeeddateFemaleYear_5242 = 5242,
        [ProductData(5243, "6 Month Private Mode (Apple IAP)", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateModeAppleIap_5243 = 5243,
        [ProductData(5244, "3 Month Private Mode (Apple IAP)", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateModeAppleIap_5244 = 5244,
        [ProductData(5245, "1 Month Private Mode (Apple IAP)", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateModeAppleIap_5245 = 5245,
        [ProductData(5300, "LatAm.EMail.30", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamEmail_5300 = 5300,
        [ProductData(5301, "LatAm.EMail.90", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamEmail_5301 = 5301,
        [ProductData(5302, "LatAm.EMail.180", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamEmail_5302 = 5302,
        [ProductData(5303, "LatAm.EMail.90.Installment", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamEmailInstallment_5303 = 5303,
        [ProductData(5304, "LatAm.EMail.180.Installment", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamEmailInstallment_5304 = 5304,
        [ProductData(5305, "LatAm.MRA.30", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamMra_5305 = 5305,
        [ProductData(5306, "LatAm.MRA.90", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamMra_5306 = 5306,
        [ProductData(5307, "LatAm.MRA.180", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamMra_5307 = 5307,
        [ProductData(5308, "LatAm.MRA.90.Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamMraInstallment_5308 = 5308,
        [ProductData(5309, "LatAm.MRA.180.Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamMraInstallment_5309 = 5309,
        [ProductData(5310, "LatAm.RFF.30", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamRff_5310 = 5310,
        [ProductData(5311, "LatAm.RFF.90", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamRff_5311 = 5311,
        [ProductData(5312, "LatAm.RFF.180", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        LatamRff_5312 = 5312,
        [ProductData(5313, "LatAm.RFF.90.Installment", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamRffInstallment_5313 = 5313,
        [ProductData(5314, "LatAm.RFF.180.Installment", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        LatamRffInstallment_5314 = 5314,
        [ProductData(5315, "1 Month Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthIntuition_5315 = 5315,
        [ProductData(5316, "3 Month Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthIntuition_5316 = 5316,
        [ProductData(5317, "6 Month Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthIntuition_5317 = 5317,
        [ProductData(5318, "12 Month Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthIntuition_5318 = 5318,
        [ProductData(5319, "1 Month Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthInvisibility_5319 = 5319,
        [ProductData(5320, "3 Month Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthInvisibility_5320 = 5320,
        [ProductData(5321, "6 Month Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthInvisibility_5321 = 5321,
        [ProductData(5322, "12 Month Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthInvisibility_5322 = 5322,
        [ProductData(5323, "1 Month Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMagnetism_5323 = 5323,
        [ProductData(5324, "3 Month Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMagnetism_5324 = 5324,
        [ProductData(5325, "6 Month Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMagnetism_5325 = 5325,
        [ProductData(5326, "12 Month Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMagnetism_5326 = 5326,
        [ProductData(5327, "1 Month Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthSuperStrength_5327 = 5327,
        [ProductData(5328, "3 Month Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthSuperStrength_5328 = 5328,
        [ProductData(5329, "6 Month Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthSuperStrength_5329 = 5329,
        [ProductData(5330, "12 Month Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthSuperStrength_5330 = 5330,
        [ProductData(5331, "6 Month Base Subscription (PP Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionPpAppleIap_5331 = 5331,
        [ProductData(5332, "6 Month Message Read Alerts (PP Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsPpAppleIap_5332 = 5332,
        [ProductData(5333, "3 Month Base Subscription (PP Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionPpAppleIap_5333 = 5333,
        [ProductData(5334, "3 Month Message Read Alerts (PP Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsPpAppleIap_5334 = 5334,
        [ProductData(5335, "1 Month Base Subscription (PP Apple IAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionPpAppleIap_5335 = 5335,
        [ProductData(5336, "1 Month Message Read Alerts (PP Apple IAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsPpAppleIap_5336 = 5336,
        [ProductData(5337, "6 Month Reply For Free (PP Apple IAP)", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreePpAppleIap_5337 = 5337,
        [ProductData(5338, "3 Month Reply For Free (PP Apple IAP)", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreePpAppleIap_5338 = 5338,
        [ProductData(5339, "1 Month Reply For Free (PP Apple IAP)", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreePpAppleIap_5339 = 5339,
        [ProductData(5340, "Match IQ", (ProductFeatureType)37, false, 1, 1440, new ProductAttributeTypeEnum[] {  })]
        MatchIq_5340 = 5340,
        [ProductData(5341, "2 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_5341 = 5341,
        [ProductData(5342, "2 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_5342 = 5342,
        [ProductData(5343, "2 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_5343 = 5343,
        [ProductData(5344, "2 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_5344 = 5344,
        [ProductData(5345, "2 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_5345 = 5345,
        [ProductData(5346, "2 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_5346 = 5346,
        [ProductData(5347, "2 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_5347 = 5347,
        [ProductData(5348, "Match Verification", (ProductFeatureType)38, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchVerification_5348 = 5348,
        [ProductData(5349, "2 Month Base Subscription (Apple iAP)", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_5349 = 5349,
        [ProductData(5350, "2 Month Message Read Alerts (Apple iAP)", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_5350 = 5350,
        [ProductData(5351, "3 Month Base Subscription: 30-Day Renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionDayRenewal_5351 = 5351,
        [ProductData(5352, "6 Month Base Subscription: 30-Day Renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionDayRenewal_5352 = 5352,
        [ProductData(5353, "3 Month  Message Read Alerts : 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlertsDayRenewal_5353 = 5353,
        [ProductData(5354, "6 Month  Message Read Alerts : 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlertsDayRenewal_5354 = 5354,
        [ProductData(5355, "3 Month Reply for Free: 30-Day Renewal", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFreeDayRenewal_5355 = 5355,
        [ProductData(5356, "6 Month Reply for Free: 30-Day Renewal", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFreeDayRenewal_5356 = 5356,
        [ProductData(5357, "3 Month Base Subscription: 30-Day Renewal: Installment", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthBaseSubscriptionDayRenewalInstallment_5357 = 5357,
        [ProductData(5358, "6 Month Base Subscription: 30-Day Renewal: Installment", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthBaseSubscriptionDayRenewalInstallment_5358 = 5358,
        [ProductData(5359, "3 Month  Message Read Alerts : 30-Day Renewal: Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthMessageReadAlertsDayRenewalInstallment_5359 = 5359,
        [ProductData(5360, "6 Month  Message Read Alerts : 30-Day Renewal: Installment", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthMessageReadAlertsDayRenewalInstallment_5360 = 5360,
        [ProductData(5361, "3 Month Reply for Free: 30-Day Renewal: Installment", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthReplyForFreeDayRenewalInstallment_5361 = 5361,
        [ProductData(5362, "6 Month Reply for Free: 30-Day Renewal: Installment", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)8 })]
        MonthReplyForFreeDayRenewalInstallment_5362 = 5362,
        [ProductData(18001, "1 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18001 = 18001,
        [ProductData(18002, "3 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18002 = 18002,
        [ProductData(18003, "7 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18003 = 18003,
        [ProductData(18004, "10 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18004 = 18004,
        [ProductData(18005, "14 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18005 = 18005,
        [ProductData(18006, "30 Day Free Trial", (ProductFeatureType)0, false, 0, 0, new ProductAttributeTypeEnum[] {  })]
        DayFreeTrial_18006 = 18006,
        [ProductData(18007, "1 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_18007 = 18007,
        [ProductData(18008, "1 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_18008 = 18008,
        [ProductData(18009, "1 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_18009 = 18009,
        [ProductData(18010, "1 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_18010 = 18010,
        [ProductData(18011, "1 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_18011 = 18011,
        [ProductData(18012, "1 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_18012 = 18012,
        [ProductData(18013, "1 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_18013 = 18013,
        [ProductData(18014, "1 Month AddOn Bundle: Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleIntuition_18014 = 18014,
        [ProductData(18015, "1 Month AddOn Bundle: Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleInvisibility_18015 = 18015,
        [ProductData(18016, "1 Month AddOn Bundle: Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleMagnetism_18016 = 18016,
        [ProductData(18017, "1 Month AddOn Bundle: Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleSuperStrength_18017 = 18017,
        [ProductData(18018, "2 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_18018 = 18018,
        [ProductData(18019, "2 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_18019 = 18019,
        [ProductData(18020, "2 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_18020 = 18020,
        [ProductData(18021, "2 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_18021 = 18021,
        [ProductData(18022, "2 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_18022 = 18022,
        [ProductData(18023, "2 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_18023 = 18023,
        [ProductData(18024, "2 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_18024 = 18024,
        [ProductData(18025, "3 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_18025 = 18025,
        [ProductData(18026, "3 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_18026 = 18026,
        [ProductData(18027, "3 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_18027 = 18027,
        [ProductData(18028, "3 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_18028 = 18028,
        [ProductData(18029, "3 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_18029 = 18029,
        [ProductData(18030, "3 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_18030 = 18030,
        [ProductData(18031, "3 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_18031 = 18031,
        [ProductData(18032, "3 Month AddOn Bundle: Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleIntuition_18032 = 18032,
        [ProductData(18033, "3 Month AddOn Bundle: Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleInvisibility_18033 = 18033,
        [ProductData(18034, "3 Month AddOn Bundle: Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleMagnetism_18034 = 18034,
        [ProductData(18035, "3 Month AddOn Bundle: Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleSuperStrength_18035 = 18035,
        [ProductData(18036, "6 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_18036 = 18036,
        [ProductData(18037, "6 Month Base Subscription: Guarantee", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)2 })]
        MonthBaseSubscriptionGuarantee_18037 = 18037,
        [ProductData(18038, "6 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_18038 = 18038,
        [ProductData(18039, "6 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_18039 = 18039,
        [ProductData(18040, "6 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_18040 = 18040,
        [ProductData(18041, "6 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_18041 = 18041,
        [ProductData(18042, "6 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_18042 = 18042,
        [ProductData(18043, "6 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_18043 = 18043,
        [ProductData(18044, "6 Month AddOn Bundle: Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleIntuition_18044 = 18044,
        [ProductData(18045, "6 Month AddOn Bundle: Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleInvisibility_18045 = 18045,
        [ProductData(18046, "6 Month AddOn Bundle: Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleMagnetism_18046 = 18046,
        [ProductData(18047, "6 Month AddOn Bundle: Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleSuperStrength_18047 = 18047,
        [ProductData(18048, "12 Month Base Subscription", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscription_18048 = 18048,
        [ProductData(18049, "12 Month Highlighted Profile", (ProductFeatureType)3, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthHighlightedProfile_18049 = 18049,
        [ProductData(18050, "12 Month First Impressions", (ProductFeatureType)5, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthFirstImpressions_18050 = 18050,
        [ProductData(18051, "12 Month  Message Read Alerts ", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlerts_18051 = 18051,
        [ProductData(18052, "12 Month matchPhone", (ProductFeatureType)12, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMatchphone_18052 = 18052,
        [ProductData(18053, "12 Month Reply for Free", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFree_18053 = 18053,
        [ProductData(18054, "12 Month Private Mode", (ProductFeatureType)29, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthPrivateMode_18054 = 18054,
        [ProductData(18055, "12 Month AddOn Bundle: Intuition", (ProductFeatureType)33, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleIntuition_18055 = 18055,
        [ProductData(18056, "12 Month AddOn Bundle: Invisibility", (ProductFeatureType)34, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleInvisibility_18056 = 18056,
        [ProductData(18057, "12 Month AddOn Bundle: Magnetism", (ProductFeatureType)35, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleMagnetism_18057 = 18057,
        [ProductData(18058, "12 Month AddOn Bundle: Super Strength", (ProductFeatureType)36, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthAddonBundleSuperStrength_18058 = 18058,
        [ProductData(18059, "1 Month Base Subscription: 7 Day Free Trial: Apple IAP", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionDayFreeTrialAppleIap_18059 = 18059,
        [ProductData(18060, "1 Month Base Subscription: Apple IAP", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_18060 = 18060,
        [ProductData(18061, "1 Month  Message Read Alerts : Apple IAP", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_18061 = 18061,
        [ProductData(18062, "1 Month Reply for Free: Apple IAP", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreeAppleIap_18062 = 18062,
        [ProductData(18063, "3 Month Base Subscription: Apple IAP", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_18063 = 18063,
        [ProductData(18064, "3 Month  Message Read Alerts : Apple IAP", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_18064 = 18064,
        [ProductData(18065, "3 Month Reply for Free: Apple IAP", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreeAppleIap_18065 = 18065,
        [ProductData(18066, "6 Month Base Subscription: Apple IAP", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_18066 = 18066,
        [ProductData(18067, "6 Month  Message Read Alerts : Apple IAP", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_18067 = 18067,
        [ProductData(18068, "6 Month Reply for Free: Apple IAP", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreeAppleIap_18068 = 18068,
        [ProductData(18069, "Profile Consulting", (ProductFeatureType)16, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        ProfileConsulting_18069 = 18069,
        [ProductData(18070, "Match Event Member Tickets", (ProductFeatureType)21, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventMemberTickets_18070 = 18070,
        [ProductData(18071, "Match Event Guest Tickets - Male", (ProductFeatureType)22, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventGuestTicketsMale_18071 = 18071,
        [ProductData(18072, "Match Event Guest Tickets - Female", (ProductFeatureType)23, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchEventGuestTicketsFemale_18072 = 18072,
        [ProductData(18073, "TopSpot", (ProductFeatureType)26, false, 1, 60, new ProductAttributeTypeEnum[] {  })]
        Topspot_18073 = 18073,
        [ProductData(18074, "TopSpot 10 Pack", (ProductFeatureType)26, false, 10, 60, new ProductAttributeTypeEnum[] {  })]
        TopspotPack_18074 = 18074,
        [ProductData(18075, "TopSpot 5 Pack", (ProductFeatureType)26, false, 5, 60, new ProductAttributeTypeEnum[] {  })]
        TopspotPack_18075 = 18075,
        [ProductData(18076, "Stealth", (ProductFeatureType)27, false, 1, 1440, new ProductAttributeTypeEnum[] {  })]
        Stealth_18076 = 18076,
        [ProductData(18077, "MatchMe", (ProductFeatureType)30, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        Matchme_18077 = 18077,
        [ProductData(18078, "Speed Verify", (ProductFeatureType)32, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        SpeedVerify_18078 = 18078,
        [ProductData(18079, "Match IQ", (ProductFeatureType)37, false, 1, 1440, new ProductAttributeTypeEnum[] {  })]
        MatchIq_18079 = 18079,
        [ProductData(18080, "Match Verification", (ProductFeatureType)38, false, 1, 0, new ProductAttributeTypeEnum[] {  })]
        MatchVerification_18080 = 18080,
        [ProductData(18081, "2 Month Base Subscription: Apple IAP", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthBaseSubscriptionAppleIap_18081 = 18081,
        [ProductData(18082, "2 Month  Message Read Alerts : Apple IAP", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsAppleIap_18082 = 18082,
        [ProductData(18083, "2 Month Reply For Free: Apple IAP", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthReplyForFreeAppleIap_18083 = 18083,
        [ProductData(18084, "3 Month Base Subscription: 30-Day Renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionDayRenewal_18084 = 18084,
        [ProductData(18085, "3 Month  Message Read Alerts : 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlertsDayRenewal_18085 = 18085,
        [ProductData(18086, "3 Month Reply for Free: 30-Day Renewal", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFreeDayRenewal_18086 = 18086,
        [ProductData(18087, "6 Month Base Subscription: 30-Day Renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionDayRenewal_18087 = 18087,
        [ProductData(18088, "6 Month  Message Read Alerts : 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlertsDayRenewal_18088 = 18088,
        [ProductData(18089, "6 Month Reply for Free: 30-Day Renewal", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthReplyForFreeDayRenewal_18089 = 18089,
        [ProductData(18090, "1 Month  Message Read Alerts : 7 Day Free Trial: Apple IAP", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
        MonthMessageReadAlertsDayFreeTrialAppleIap_18090 = 18090,
        [ProductData(18091, "12 Month Base Subscription: 30-Day Renewal", (ProductFeatureType)1, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthBaseSubscriptionDayRenewal_18091 = 18091,
        [ProductData(18092, "12 Month  Message Read Alerts : 30-Day Renewal", (ProductFeatureType)9, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
        MonthMessageReadAlertsDayRenewal_18092 = 18092,
        [ProductData(18093, "12 Month Reply for Free: 30-Day Renewal", (ProductFeatureType)28, true, 0, 0, new ProductAttributeTypeEnum[] {  })]
    MonthReplyForFreeDayRenewal_18093 = 18093,
    [ProductData(18094, "Email Communication: Non-Renewing Comp", (ProductFeatureType)1, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    EmailCommunicationNonRenewingComp_18094 = 18094,
    [ProductData(18095, "Highlighted Profile: Non-Renewing Comp", (ProductFeatureType)3, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    HighlightedProfileNonRenewingComp_18095 = 18095,
    [ProductData(18096, "First Impressions: Non-Renewing Comp", (ProductFeatureType)5, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    FirstImpressionsNonRenewingComp_18096 = 18096,
    [ProductData(18097, " Message Read Alerts : Non-Renewing Comp", (ProductFeatureType)9, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    MessageReadAlertsNonRenewingComp_18097 = 18097,
    [ProductData(18098, "matchPhone: Non-Renewing Comp", (ProductFeatureType)12, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    MatchphoneNonRenewingComp_18098 = 18098,
    [ProductData(18099, "Reply For Free: Non-Renewing Comp", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    ReplyForFreeNonRenewingComp_18099 = 18099,
    [ProductData(18100, "Private Mode: Non-Renewing Comp", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { })]
    PrivateModeNonRenewingComp_18100 = 18100,
    [ProductData(18101, "SuperLike - 5 Pack", (ProductFeatureType)39, false, 5, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18101 = 18101,
    [ProductData(18102, "SuperLike - 15 Pack", (ProductFeatureType)39, false, 15, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18102 = 18102,
    [ProductData(18103, "SuperLike - 30 Pack", (ProductFeatureType)39, false, 30, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18103 = 18103,
    [ProductData(18104, "ProfilePro Lite", (ProductFeatureType)17, false, 1, 0, new ProductAttributeTypeEnum[] { })]
    ProfileproLite_18104 = 18104,
    [ProductData(18105, "DateCoach a la carte", (ProductFeatureType)4, false, 1, 0, new ProductAttributeTypeEnum[] { })]
    DatecoachALaCarte_18105 = 18105,
    [ProductData(18106, "DateCoach 2 Sessions", (ProductFeatureType)4, true, 2, 0, new ProductAttributeTypeEnum[] { })]
    DatecoachSessions_18106 = 18106,
    [ProductData(18107, "DateCoach 4 Sessions", (ProductFeatureType)4, true, 4, 0, new ProductAttributeTypeEnum[] { })]
    DatecoachSessions_18107 = 18107,
    [ProductData(18108, "SuperLike - 1 Pack", (ProductFeatureType)39, false, 1, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18108 = 18108,
    [ProductData(18109, "1 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18109 = 18109,
    [ProductData(18110, "2 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18110 = 18110,
    [ProductData(18111, "3 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18111 = 18111,
    [ProductData(18112, "4 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18112 = 18112,
    [ProductData(18113, "5 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18113 = 18113,
    [ProductData(18114, "6 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18114 = 18114,
    [ProductData(18115, "7 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18115 = 18115,
    [ProductData(18116, "8 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18116 = 18116,
    [ProductData(18117, "9 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18117 = 18117,
    [ProductData(18118, "01 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18118 = 18118,
    [ProductData(18119, "11 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18119 = 18119,
    [ProductData(18120, "12 Month Reply For Free Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthReplyForFreeNonRenewableAppleIap_18120 = 18120,
    [ProductData(18121, "1 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18121 = 18121,
    [ProductData(18122, "2 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18122 = 18122,
    [ProductData(18123, "3 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18123 = 18123,
    [ProductData(18124, "4 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18124 = 18124,
    [ProductData(18125, "5 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18125 = 18125,
    [ProductData(18126, "6 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18126 = 18126,
    [ProductData(18127, "7 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18127 = 18127,
    [ProductData(18128, "8 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18128 = 18128,
    [ProductData(18129, "9 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18129 = 18129,
    [ProductData(18130, "01 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18130 = 18130,
    [ProductData(18131, "11 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18131 = 18131,
    [ProductData(18132, "12 Month PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    MonthPrivatemodeNonRenewableAppleIap_18132 = 18132,
    [ProductData(18134, "1W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18134 = 18134,
    [ProductData(18135, "2W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18135 = 18135,
    [ProductData(18136, "3W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18136 = 18136,
    [ProductData(18137, "4W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18137 = 18137,
    [ProductData(18138, "5W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18138 = 18138,
    [ProductData(18139, "6W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18139 = 18139,
    [ProductData(18140, "7W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18140 = 18140,
    [ProductData(18141, "8W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18141 = 18141,
    [ProductData(18142, "9W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18142 = 18142,
    [ProductData(18143, "10W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18143 = 18143,
    [ProductData(18144, "11W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18144 = 18144,
    [ProductData(18145, "12W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18145 = 18145,
    [ProductData(18146, "13W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18146 = 18146,
    [ProductData(18147, "14W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18147 = 18147,
    [ProductData(18148, "15W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18148 = 18148,
    [ProductData(18149, "16W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18149 = 18149,
    [ProductData(18150, "17W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18150 = 18150,
    [ProductData(18151, "18W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18151 = 18151,
    [ProductData(18152, "19W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18152 = 18152,
    [ProductData(18153, "20W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18153 = 18153,
    [ProductData(18154, "21W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18154 = 18154,
    [ProductData(18155, "22W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18155 = 18155,
    [ProductData(18156, "23W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18156 = 18156,
    [ProductData(18157, "24W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18157 = 18157,
    [ProductData(18158, "25W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18158 = 18158,
    [ProductData(18159, "26W RFF Non Renewable: Apple IAP", (ProductFeatureType)28, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WRffNonRenewableAppleIap_18159 = 18159,
    [ProductData(18160, "1W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18160 = 18160,
    [ProductData(18161, "2W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18161 = 18161,
    [ProductData(18162, "3W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18162 = 18162,
    [ProductData(18163, "4W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18163 = 18163,
    [ProductData(18164, "5W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18164 = 18164,
    [ProductData(18165, "6W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18165 = 18165,
    [ProductData(18166, "7W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18166 = 18166,
    [ProductData(18167, "8W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18167 = 18167,
    [ProductData(18168, "9W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18168 = 18168,
    [ProductData(18169, "10W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18169 = 18169,
    [ProductData(18170, "11W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18170 = 18170,
    [ProductData(18171, "12W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18171 = 18171,
    [ProductData(18172, "13W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18172 = 18172,
    [ProductData(18173, "14W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18173 = 18173,
    [ProductData(18174, "15W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18174 = 18174,
    [ProductData(18175, "16W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18175 = 18175,
    [ProductData(18176, "17W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18176 = 18176,
    [ProductData(18177, "18W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18177 = 18177,
    [ProductData(18178, "19W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18178 = 18178,
    [ProductData(18179, "20W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18179 = 18179,
    [ProductData(18180, "21W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18180 = 18180,
    [ProductData(18181, "22W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18181 = 18181,
    [ProductData(18182, "23W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18182 = 18182,
    [ProductData(18183, "24W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18183 = 18183,
    [ProductData(18184, "25W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18184 = 18184,
    [ProductData(18185, "26W PrivateMode Non Renewable: Apple IAP", (ProductFeatureType)29, false, 0, 0, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)7 })]
    WPrivatemodeNonRenewableAppleIap_18185 = 18185,
    [ProductData(18186, "SuperLike - 10 Pack", (ProductFeatureType)39, false, 10, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18186 = 18186,
    [ProductData(18187, "SuperLike - 25 Pack", (ProductFeatureType)39, false, 25, 0, new ProductAttributeTypeEnum[] { })]
    SuperlikePack_18187 = 18187,
    [ProductData(18188, "Rushhour Boost", (ProductFeatureType)26, false, 1, 180, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)9 })]
    RushhourBoost_18188 = 18188,
    [ProductData(18189, "BoostRFF", (ProductFeatureType)26, false, 1, 60, new ProductAttributeTypeEnum[] { (ProductAttributeTypeEnum)10 })]
    Boostrff_18189 = 18189,
}
