<Query Kind="Program" />

void Main()
{
    InstallmentTypeEnum x = (InstallmentTypeEnum)12;
    
    Console.WriteLine(Enum.IsDefined(typeof(InstallmentTypeEnum), (InstallmentTypeEnum)12));
    Console.WriteLine(Enum.IsDefined(typeof(InstallmentTypeEnum), (InstallmentTypeEnum)1000));
    
    Console.WriteLine(GetInstallmentTypeData((InstallmentTypeEnum)1000)?.NumberOfInstallments);
    Console.WriteLine(GetInstallmentTypeData((InstallmentTypeEnum)1)?.NumberOfInstallments);
    
}

// Define other methods and classes here

public enum InstallmentTypeEnum
{
    /// <summary>
    /// 1000 - Billed Every 14 Days
    /// </summary>
    [InstallmentTypeData(14, 4, true, "Billed Every 14 Days")]
    BilledEvery14Days = 1000,
}

    public static IEnumerable<InstallmentTypeEnum> GetAllPaymentMethodTypes()
        => Enum.GetValues(typeof(InstallmentTypeEnum)).Cast<InstallmentTypeEnum>();

    public static InstallmentTypeDataAttribute GetInstallmentTypeData(
        InstallmentTypeEnum installmentType)
    {
        FieldInfo fi = installmentType.GetType().GetField(installmentType.ToString());
        if (fi == null)
            return null;
        InstallmentTypeDataAttribute[] attributes =
            (InstallmentTypeDataAttribute[])fi.GetCustomAttributes(typeof(InstallmentTypeDataAttribute), false);
        return (attributes != null && attributes.Length > 0)
            ? attributes[0]
            : null;
    }

    public class InstallmentTypeDataAttribute : Attribute
    {
        public short InstallmentFrequency { get; }
        public byte NumberOfInstallments { get; }
        public bool IsActive { get; }
        public string Description { get; set; }

        public InstallmentTypeDataAttribute(
            short installmentFrequency,
            byte numberOfInstallments,
            bool isActive,
            string description)
        {
            InstallmentFrequency = installmentFrequency;
            NumberOfInstallments = numberOfInstallments;
            IsActive = isActive;
            Description = description;
        }
        public override bool Equals(object obj)
        {
            if (obj == this)
                return true;

            InstallmentTypeDataAttribute other = obj as InstallmentTypeDataAttribute;
            return (other != null)
                && other.InstallmentFrequency == this.InstallmentFrequency
                && other.NumberOfInstallments == this.NumberOfInstallments
                && other.IsActive == this.IsActive;
        }

        public override int GetHashCode() => base.GetHashCode();
        public override bool IsDefaultAttribute() => this.InstallmentFrequency <= 0;
    }