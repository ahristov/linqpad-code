<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
</Query>

void Main()
{
	const string testSpec = "1605223143|1234|3";
	int userId = 332407527;
	
	int falseResults = 0;
	int trueResults = 0;
	
	for (int i = 1; i <= 10000; i++)
	{
		bool testResult = IsInUserIdHashTest(testSpec, userId);
		if (testResult)
		{
			//Console.WriteLine($"{userId} -> {testResult}");
			trueResults++;
		}
		else
		{
			falseResults++;
		}
		userId--;
	}

	Console.WriteLine($"In test: {trueResults}; Not in test: {falseResults}");
}

// Define other methods and classes here

private static bool IsInUserIdHashTest(string userIdHashTest, int userId)
{
	// If no User ID hash test is specified, then the test is 100%.
	if (string.IsNullOrWhiteSpace(userIdHashTest))
		return true;

	var parts = userIdHashTest.Split('|');
	if (parts.Length == 3)
	{
		var tester = new UserIdHashEvaluator();
		tester.Init($"{parts[1]}|{parts[2]}");

		var isInTest = tester.Evaluate(userId, parts[0]);
		return isInTest;
	}

	// The provided User ID hash test string is malfoirmed.
	return false;
}


public class UserIdHashEvaluator
{
	private string _matchCharacters;
	private int _position;

	public void Init(string criteria)
	{
		string[] pieces = criteria.Split('|');

		if (pieces.Length != 2)
			throw new ArgumentException("Initialization parameters in incorrect format");

		_matchCharacters = pieces[0].ToLower();
		int.TryParse(pieces[1], out _position);
	}

	public bool Evaluate(int userid, string salt)
	{
		string hash = CalcuateUserIDTestHash(userid, salt).ToLower();
		return _matchCharacters.IndexOf(hash[_position - 1]) >= 0;
	}


	//DO NOT MODIFY, Mirrors hash in MCoreAssembly
	private static string CalcuateUserIDTestHash(int userId, string testName)
	{
		string plainText = userId + testName;
		SHA1 cryptoProvider = new SHA1CryptoServiceProvider();
		byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
		byte[] cipherTextBytes = cryptoProvider.ComputeHash(plainTextBytes);
		StringBuilder bufferBuilder = new StringBuilder("0x", 60);
		foreach (byte currentByte in cipherTextBytes)
		{
			bufferBuilder.Append(currentByte.ToString("x2"));
		}
		return bufferBuilder.ToString();
	}
}

