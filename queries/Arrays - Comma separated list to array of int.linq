<Query Kind="Program" />

void Main()
{
	foreach(var s in new string[] {
		null, "", "a", "s23", "1111,cd,2222,,,,,,3333,,,,,,,", "12,28,29"
	})
	{
		Console.WriteLine(GetFeaturesFromList(s));
		foreach (byte featureId in new byte[] { 12,28,29, 1 })
		{
			Console.WriteLine(PromoApplicableTo(s, featureId));
		}
	}
	
}

static bool PromoApplicableTo(string prodFeatureList, byte featureId)
{
	int[] features = GetFeaturesFromList(prodFeatureList);
	var res = features.Length <= 0 || features.Any(f => f == featureId);
	return res;
}

// Define other methods and classes here
static private int[] GetFeaturesFromList(string prodFeatureList) => ("" + prodFeatureList)
    .Trim()
    .Split(new char[] { ',' })
    .Select(s => (int.TryParse(s, out int pf) ? (int?)pf : null))
    .Where(pf => pf.HasValue)
    .Select(pf => pf.Value)
    .ToArray();
	
