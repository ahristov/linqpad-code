<Query Kind="Program" />

void Main()
{
    foreach (var test in new List<Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>>
    {
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            null,
            null
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {},
            new ProductAttributeTypeEnum[]
            {}
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.None
            },
            new ProductAttributeTypeEnum[]
            {}
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing,
            },
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing,
            }
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.LoyaltyPricing,
            },
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing,
            }
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.AppleIosInAppPurchase, ProductAttributeTypeEnum.LoyaltyPricing,
            },
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.AppleIosInAppPurchase,
            }
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            { 
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.MonthGuaranteeProgram, ProductAttributeTypeEnum.CsaFreeAddOnExistingSubs, ProductAttributeTypeEnum.OneTimeEventServiceFeeDoNotDisplayOnRateCard,
                ProductAttributeTypeEnum.UpsellProductDoNotDisplayOnRateCard, ProductAttributeTypeEnum.Installments, ProductAttributeTypeEnum.AppleIosInAppPurchase, ProductAttributeTypeEnum.MonthlyInstallmentsCollectedByPaymentProcessor, ProductAttributeTypeEnum.RushhourBoost,
            },
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.MonthGuaranteeProgram, ProductAttributeTypeEnum.CsaFreeAddOnExistingSubs, ProductAttributeTypeEnum.OneTimeEventServiceFeeDoNotDisplayOnRateCard,
                ProductAttributeTypeEnum.UpsellProductDoNotDisplayOnRateCard, ProductAttributeTypeEnum.Installments, ProductAttributeTypeEnum.AppleIosInAppPurchase, ProductAttributeTypeEnum.MonthlyInstallmentsCollectedByPaymentProcessor, ProductAttributeTypeEnum.RushhourBoost,
            }
        ),
        new Tuple<ProductAttributeTypeEnum[], ProductAttributeTypeEnum[]>(
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.MonthGuaranteeProgram, ProductAttributeTypeEnum.CsaFreeAddOnExistingSubs, ProductAttributeTypeEnum.OneTimeEventServiceFeeDoNotDisplayOnRateCard,
                ProductAttributeTypeEnum.UpsellProductDoNotDisplayOnRateCard, ProductAttributeTypeEnum.Installments, ProductAttributeTypeEnum.AppleIosInAppPurchase, ProductAttributeTypeEnum.MonthlyInstallmentsCollectedByPaymentProcessor, ProductAttributeTypeEnum.RushhourBoost,
            },
            new ProductAttributeTypeEnum[]
            {
                ProductAttributeTypeEnum.LoyaltyPricing, ProductAttributeTypeEnum.MonthGuaranteeProgram, ProductAttributeTypeEnum.CsaFreeAddOnExistingSubs, 
                ProductAttributeTypeEnum.UpsellProductDoNotDisplayOnRateCard, ProductAttributeTypeEnum.Installments, ProductAttributeTypeEnum.AppleIosInAppPurchase, ProductAttributeTypeEnum.MonthlyInstallmentsCollectedByPaymentProcessor, ProductAttributeTypeEnum.RushhourBoost,
            }
        ),


    }
    )
    {
        var sum1 = GetBitMask(test?.Item1 ?? new ProductAttributeTypeEnum[] {});
        var sum2 = GetBitMask(test?.Item2 ?? new ProductAttributeTypeEnum[] {});

        Console.WriteLine($"This {sum1} == that {sum2} -> {sum1 == sum2}");
    }
}

private static long GetBitMask(ProductAttributeTypeEnum[] items)
{
    var sum = items
                .Where(x => (int)x > 0)
                .Select(x => (int)x)
                .Distinct()
                .Sum(x => Math.Pow(2, x - 1));
    return (long)sum;        
}

// Define other methods and classes here

[Serializable]
public enum ProductAttributeTypeEnum
{
    None = 0,
    LoyaltyPricing = 1,
    MonthGuaranteeProgram = 2,
    CsaFreeAddOnExistingSubs = 3,
    OneTimeEventServiceFeeDoNotDisplayOnRateCard = 4,
    UpsellProductDoNotDisplayOnRateCard = 5,
    Installments = 6,
    AppleIosInAppPurchase = 7,
    MonthlyInstallmentsCollectedByPaymentProcessor = 8,
    RushhourBoost = 9,
}