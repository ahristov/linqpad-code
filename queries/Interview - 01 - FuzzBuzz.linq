<Query Kind="Program" />

/*
  Write a program that prints the numbers from 1 to 100.
  But for multiples of three print “Fizz” instead of the number
  and for the multiples of five print “Buzz”.
  For numbers which are multiples of both three and five print “FizzBuzz”.
*/
void Main()
{
    FuzzBuzz4(100, new List<(int, string)> {
        (3, "Fuzz"),
        (5, "Buzz") 
    });
}

/*
 * Initial implementation.
 */
void FuzzBuss1()
{
    for (int i = 1; i <= 100; i++)
    {
        if (i % 3 == 0 && i % 5 == 0)
            Console.WriteLine("FuzzBuzz");
        else if (i % 3 == 0)
            Console.WriteLine("Fuzz");
        else if (i % 5 == 0)
            Console.WriteLine("Buzz");
        else
            Console.WriteLine(i);
    }
}

/*
 * Refactor to precalculated variables.
 */
void FuzzBuzz2()
{
    for (int i = 1; i <= 100; i++)
    {
        var fuzz = i % 3 == 0;
        var buzz = i % 5 == 0;
        if (fuzz && buzz)
            Console.WriteLine("FuzzBuzz");
        else if (fuzz)
            Console.WriteLine("Fuzz");
        else if (buzz)
            Console.WriteLine("Buzz");
        else
            Console.WriteLine(i);
    }
}

/*
 * DRY strings, divisible.
 */
void FuzzBuzz3()
{
    const string FUZZ = "fuzz";
    const string BUZZ = "buzz";

    bool divisible_by(int numerator, int denominator)
        => numerator % denominator == 0;

    for (int i = 1; i <= 100; i++)
    {
        var fuzz = divisible_by(i, 3);
        var buzz = divisible_by(i, 5);
        if (fuzz && buzz)
            Console.WriteLine($"{FUZZ}{BUZZ}");
        else if (fuzz)
            Console.WriteLine($"{FUZZ}");
        else if (buzz)
            Console.WriteLine($"{BUZZ}");
        else
            Console.WriteLine(i);
    }
}

/*
 * Parametrization: range and words.
 */
void FuzzBuzz4(int range, IEnumerable<(int, string)> buzzWords)
{
    bool divisible_by(int numerator, int denominator)
        => numerator % denominator == 0;

    StringBuilder sb = new StringBuilder();
    for (int i = 1; i <= 100; i++)
    {
        sb.Clear();
        foreach ((int denominator, string buzzWord) in buzzWords)
        {
            if (divisible_by(i, denominator))
            {
                sb.Append(buzzWord);
            }
        }
        Console.WriteLine(sb.Length > 0
            ? sb.ToString()
            : i.ToString());
    }
    
}