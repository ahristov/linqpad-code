<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>System.Data.SqlClient</Namespace>
  <Namespace>System.Net</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
</Query>

void Main()
{
	//Trust all certificates
	System.Net.ServicePointManager.ServerCertificateValidationCallback =
		((sender, certificate, chain, sslPolicyErrors) => true);

	// PullFailedReceipts();
	// FetchLatestReceipts();
	ExtractLatestReceiptInfoData();
}

const string CNN_STR = "Server=MARP03\\MAREPORT01;Initial Catalog=WorkDB;Integrated Security=true;";
const int TIMEOUT = 1000;

private static string GetLatestReceiptElement(string receipt)
{
	JObject json = JObject.Parse(receipt);
	var res = json["receipt"]["latest_receipt_info"].ToString();
	return res;
}

private static string FetchLatestReceipt(int userId, string receipt)
{
	WebRequest request = WebRequest.Create("https://10.10.128.151/api/v1/inapp/verifyReceipt/apple/purchaseFlow");
	request.Method = "POST";
	request.ContentType = "application/json";

	string json = $@"
		{{
			""userId"" : 306057642,
			""platformId"": 1,
			""sessionId"" : ""00000000-0000-0000-0000-000000000000"",
			""excludeOldTransactions"": true,
			""receipt"": ""{receipt}""
		}}
	";
	byte[] byteArray = Encoding.UTF8.GetBytes(json);
	request.ContentLength = byteArray.Length;

	using (Stream dataStream = request.GetRequestStream())
	{
		dataStream.Write(byteArray, 0, byteArray.Length);
		dataStream.Close();
		dataStream.Dispose();
	}
	
	WebResponse response = request.GetResponse();
	HttpWebResponse webResponse = (HttpWebResponse)response;

	if (webResponse.StatusCode != HttpStatusCode.OK)
	{
		Console.WriteLine($"Error: {userId} - {webResponse.StatusCode} {webResponse.StatusDescription}");
		return null;
	}

	using (Stream dataStream = response.GetResponseStream())
	{
		StreamReader reader = new StreamReader(dataStream);
		string latestReceiptDecoded = reader.ReadToEnd();
		return latestReceiptDecoded;
	}
	
}
private static void UpdateLatestReceiptDecoded(int userId, int processorLogId, string latestReceiptDecoded, string latestReceiptInfo)
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = $@"
update ReceiptFailed
set
	 LatestReceiptDecoded = '{latestReceiptDecoded}'
	,LatestReceiptInfo = '{latestReceiptInfo}'
where
	UserID = {userId}
	and LastProcessorLogID = {processorLogId}
			";
			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = TIMEOUT;
			cmd.ExecuteNonQuery();
			cmd.Dispose();
		}
		cnn.Close();
		cnn.Dispose();
	}
}

private static void FetchLatestReceipts()
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = @"
select Dt, UserID, Comment, LastProcessorLogID, LastReceipt
from ReceiptFailed
order by Dt, UserID
			";
			cmd.CommandTimeout = TIMEOUT;
			cmd.CommandType = CommandType.Text;

			using (var rdr = cmd.ExecuteReader())
			{
				while (rdr.Read())
				{
					if (DBNull.Value.Equals(rdr[3]))
					{
						continue;
					}

					DateTime dt = rdr.GetDateTime(0);
					int userId = rdr.GetInt32(1);
					string comment = rdr.GetString(2);
					int lastProcessorLogId = rdr.GetInt32(3);
					string lastReceipt = rdr.GetString(4);

					string latestReceiptDecoded = FetchLatestReceipt(userId, lastReceipt);
					string latestReceiptInfo = GetLatestReceiptElement(latestReceiptDecoded);
					UpdateLatestReceiptDecoded(userId, lastProcessorLogId, latestReceiptDecoded, latestReceiptInfo);

					Console.WriteLine(new { dt, userId, });
				}

				rdr.Close();
			}

			cmd.Dispose();
		}
		cnn.Close();
		cnn.Dispose();
	}
}

private static void DeleteReceiptInfoDataForProcessorLog(int processorLogId)
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = string.Format(@"
-- MARP
delete from WorkDB..ReceiptFailedTransactions where ProcerrosLogId = {0};
",
				processorLogId
			);

			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = TIMEOUT;
			cmd.ExecuteNonQuery();
		}
	}
}

private static void InsertReceiptInfoData(
	int processorLogId,
	DateTime? purchaseDate,
	long? transactionId,
	long? originalTransactionId,
	long? webOrderLineItemId,
	string productId)
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = string.Format(@"
-- MARP
insert into WorkDB..ReceiptFailedTransactions (ProcerrosLogId, PurchaseDate, TransactionId, OriginalTransactionId, WebOrderLineItemId, ProductId)
values
({0}, '{1}', {2}, {3}, {4}, '{5}');",
				processorLogId,
				(purchaseDate ?? DateTime.MinValue).ToString(),
				transactionId.HasValue ? transactionId.Value.ToString() : "NULL",
				originalTransactionId.HasValue ? originalTransactionId.Value.ToString() : "NULL",
				webOrderLineItemId.HasValue ? webOrderLineItemId.Value.ToString() : "NULL",
				productId ?? string.Empty
			);
	
			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = TIMEOUT;
			cmd.ExecuteNonQuery();
		}
	}
}

private static void ExtractLatestReceiptInfoData()
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = @"
-- MARP
IF OBJECT_ID(N'WorkDB..ReceiptFailedTransactions') IS NOT NULL DROP TABLE WorkDB..ReceiptFailedTransactions

create table WorkDB..ReceiptFailedTransactions
(
	ProcerrosLogId int,
	PurchaseDate Datetime,
	TransactionId bigint,
	OriginalTransactionId bigint,
	WebOrderLineItemId bigint,
	ProductId varchar(100)
)
			";
			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = TIMEOUT;
			cmd.ExecuteNonQuery();


			cmd.CommandText = @"
select LastProcessorLogID, LatestReceiptInfo
from WorkDB..ReceiptFailed
where LastProcessorLogID is not null and LatestReceiptInfo is not null
order by  LastProcessorLogID
";
			using (var rdr = cmd.ExecuteReader())
			{
				while (rdr.Read())
				{
					int processorLogId = rdr.GetInt32(0);
					
					DeleteReceiptInfoDataForProcessorLog(processorLogId);

					string receiptInfo = rdr.GetString(1);
					if (string.IsNullOrWhiteSpace(receiptInfo))
					{
						continue;
					}

					var trxs = JArray.Parse(receiptInfo)
						.Children()
						.ToList()
						.Select(trx => new {
							PurchaseDate = 
								string.IsNullOrWhiteSpace(trx["purchase_date"].ToString())
								? (DateTime?)null
								: DateTime.Parse( trx["purchase_date"].ToString().Replace(" Etc/GMT", "")),
							TransactionId = 
								string.IsNullOrWhiteSpace(trx["transaction_id"].ToString())
								? (long?)null
								: Int64.Parse(trx["transaction_id"].ToString()),
							OriginalTransactionId = 
								string.IsNullOrWhiteSpace(trx["original_transaction_id"].ToString())
								? (long?)null
								: Int64.Parse(trx["original_transaction_id"].ToString()),
							WebOrderLineItemId = 
								string.IsNullOrWhiteSpace(trx["web_order_line_item_id"].ToString())
									? (long?)null
									: Int64.Parse(trx["web_order_line_item_id"].ToString()),
							ProductId = trx["product_id"].ToString(),
						})
						.OrderByDescending(t => t.PurchaseDate);

					foreach (var trx in trxs)
					{
						Console.WriteLine(trx);
						InsertReceiptInfoData(
							processorLogId,
							trx.PurchaseDate,
							trx.TransactionId,
							trx.OriginalTransactionId,
							trx.WebOrderLineItemId,
							trx.ProductId);
					}
				}

				rdr.Close();
			}


			cmd.Dispose();
		}
		cnn.Close();
		cnn.Dispose();
	}
}

private static void PullFailedReceipts()
{
	using (var cnn = new SqlConnection(CNN_STR))
	{
		cnn.Open();
		using (SqlCommand cmd = cnn.CreateCommand())
		{
			cmd.CommandText = @"

-- MARP
IF OBJECT_ID(N'WorkDB..ReceiptFailed') IS NOT NULL DROP TABLE WorkDB..ReceiptFailed

Select distinct 
  Cast(ActivityDt as date) As Dt
, UserID
, Comment
, cast(null as int) as LastProcessorLogID
, cast(null as varchar(max)) as LastReceipt
, cast(null as varchar(max)) as LatestReceiptDecoded
, cast(null as varchar(max)) as LatestReceiptInfo
INTO WorkDB..ReceiptFailed
From CSAMatchData.dbo.csa_UserAcctActivity
Where activityID = 315
    And ActivityDate > '05/21/2020 20:00'
    And activitydate < '06/02/2020'
    And Comment Like '%attempted to replay IAP receipt%'
Order By CAst(ActivityDt as date), UserID

delete from WorkDB..ReceiptFailed where Comment not like 'User ' + CAST(UserID as varchar(100)) + ' attempted to replay%'

update WorkDB..ReceiptFailed set LastProcessorLogID = null, LastReceipt = null, LatestReceiptDecoded = null;

update WorkDB..ReceiptFailed
set LastProcessorLogID = (
	select MAX(pl.Processorlogid) from Billingdata.apple.processorlog pl
	where pl.UserID = WorkDB..ReceiptFailed.UserID 
		and pl.Receipt is not null
)

update WorkDB..ReceiptFailed
set LastReceipt = (
	select MAX(pl.Receipt) from Billingdata.apple.processorlog pl
	where pl.UserID = WorkDB..ReceiptFailed.UserID 
		and pl.ProcessorLogID = WorkDB..ReceiptFailed.LastProcessorLogID
)
			
			";
			cmd.CommandType = CommandType.Text;
			cmd.CommandTimeout = TIMEOUT;
			cmd.ExecuteNonQuery();
			cmd.Dispose();
		}
		cnn.Close();
		cnn.Dispose();
	}

}
