<Query Kind="Program" />

void Main()
{
	string a = "x";
	Console.WriteLine(a.Equals(null));
	
	string[] val = new string[]
	{
			"  Not Applicable									",
			"  American Express								",
			"  Diners Club									",
			"  Discover										",
			"  JCB (Japanese Credit)							",
			"  Master Card									",
			"  Visa											",
			"  Online Check									",
			"  Check											",
			"  Cash											",
			"  Money Order									",
			"  E-Check										",
			"  Gift Subscription Redemption					",
			"  Switch/Maestro									",
			"  PayPal											",
			"  Zeus Conversion Without Payment Information    ",
			"  Danal/Teledit									",
			"  GC Bank Transfer								",
			"  Apple IAP										",
			"  GC - konbini									",
			"  Ello											",
			"  Hipercard										",
			"  Boleto											",
			"  Debito											",
			"  DineroMail										",
			"  Amazon											",
			"  Apple Pay										",
			"  Naranja										",
			"  Shopping										",
			"  Cabal											",
			"  ArgenCard										",
			"  Cencosud										",
			"  BlackHawk Gift Card							",
	};

	foreach (var s in val)
	{
		Console.WriteLine(ToTitle(s));
	}
}

// You can define other methods, fields, classes and namespaces here
static string ToTitle(string s)
{
	return string.Join("", Regex.Split((s ?? string.Empty).Trim().ToLowerInvariant(), "[^a-z]", RegexOptions.IgnoreCase)
		.Where(x => x.Length > 0)
		.Select(x => ToName(x)));
}

static string ToName(string s)
{
	s = (""+s).Trim().ToLowerInvariant();
	return string.IsNullOrWhiteSpace(s)
		? s
		: char.ToUpperInvariant(s[0]) + s.Substring(1);
}

