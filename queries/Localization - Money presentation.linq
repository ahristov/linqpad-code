<Query Kind="Statements" />

foreach (string cultureName in new string[] {"en-US", "en-CA", "fr-CA", "fr-FR" })
{
	var cultureInfo = new System.Globalization.CultureInfo(cultureName);
	var localMoney = (123.45d).ToString("C", cultureInfo);
	Console.WriteLine("{0} -> {1}", cultureName, localMoney);
}



