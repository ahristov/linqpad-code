<Query Kind="Program">
  <NuGetReference>FluentAssertions</NuGetReference>
  <NuGetReference>NUnit</NuGetReference>
  <Namespace>FluentAssertions</Namespace>
  <Namespace>NUnit.Framework</Namespace>
  <Namespace>System.Text</Namespace>
</Query>

void Main()
{
    foreach (var testData in InstallmentsPlanCalculatorTests.TestDataSet)
    {
        new InstallmentsPlanCalculatorTests().RunTest(testData);
    }
}

// Installments calculator

public interface IInstallmentsPlanCalculator
{
    IEnumerable<InstallmentData> CalculateInstallmentPlan(
        InstallmentTypeEnum installmentType,
        DateTime orderDate,
        short productDays,
        decimal salesPrice,
        decimal renewalPrice);
}


public class InstallmentsPlanCalculator : IInstallmentsPlanCalculator
{
    public IEnumerable<InstallmentData> CalculateInstallmentPlan(
        InstallmentTypeEnum installmentType,
        DateTime orderDate,
        short productDays,
        decimal salesPrice,
        decimal renewalPrice)
    {
        var res = new List<InstallmentData>();
        DateTime installmentBillDt = orderDate.Date;
        DateTime beginDt = orderDate;
        DateTime endDt = orderDate.AddDays(productDays);

        var installmentData = installmentType.GetInstallmentTypeData();
        var provisionDaysList = SplitProvisionDays(productDays, installmentData.NumberOfInstallments, beginDt, endDt);
        var initialInstallmentAmountsList = SplitInstallmentAmounts(salesPrice, installmentData.NumberOfInstallments);
        var renewalInstallmentAmountsList = SplitInstallmentAmounts(renewalPrice, installmentData.NumberOfInstallments);

        for (var i = 0; i < installmentData.NumberOfInstallments; i++)
        {
            res.Add(new InstallmentData
            {
                ProvisionDays = provisionDaysList[i],
                InitialInstallmentAmount = initialInstallmentAmountsList[i],
                RenewalInstallmentAmount = renewalInstallmentAmountsList[i],
                InstallmentBillDt = installmentBillDt,
                BeginDt = beginDt,
                EndDt = beginDt.AddDays(provisionDaysList[i])
            });

            installmentBillDt = installmentBillDt.AddDays(installmentData.InstallmentFrequency);
            beginDt = beginDt.AddDays(provisionDaysList[i]);
        }

        return res;
    }

    private IList<short> SplitProvisionDays(short orderProvisionDays, byte termsCount, DateTime orderBeginDt, DateTime orderEndDt)
    {
        var res = new List<short>();
        DateTime calcEndDt = orderBeginDt;

        for (byte i = 0; i < termsCount; i++)
        {
            short provisionDays = (short)(orderProvisionDays / termsCount);

            if (i == termsCount - 1)
            {
                if (calcEndDt.AddDays(provisionDays) > orderEndDt)
                {
                    for (byte j = 0; j < res.Count; j++)
                    {
                        res[j]--;
                    }
                }
                
                provisionDays = (short)(orderProvisionDays - res.Sum(x => x));
            }
            
            res.Add(provisionDays);
            calcEndDt = calcEndDt.AddDays(provisionDays);
        }

        return res;
    }


    private IList<decimal> SplitInstallmentAmounts(decimal totalAmount, byte termsCount)
    {
        var res = new List<decimal>();
        decimal calculatedTotal = 0;
        
        for (byte i = 0; i < termsCount; i++)
        {
            decimal installment = Math.Round(totalAmount / termsCount, 2, MidpointRounding.AwayFromZero);

            if (i == termsCount - 1)
            {
                if (calculatedTotal + installment > totalAmount)
                {
                    // If total of installments great than totalAmount, make last installment smaller than the rest of installments.  
                    installment = totalAmount - calculatedTotal;
				}
				else if (calculatedTotal + installment < totalAmount)
				{
					// If total of installments less than totalAmount, increase previous rates, make last installment smaller than the rest of installments.  
					while (calculatedTotal + installment < totalAmount)
					{
                        calculatedTotal = 0;
                        
						for (byte j = 0; j < res.Count; j++)
						{
							res[j] += .01m;
                            calculatedTotal += res[j];
						}
                        
                        installment = totalAmount - calculatedTotal;
					}
                }
            }

            res.Add(installment);
            calculatedTotal += installment;
        }

        return res;
    }
}



// Data structures

public class InstallmentTypeDataAttribute : Attribute
{
    public short InstallmentFrequency { get; }
    public byte NumberOfInstallments { get; }
    public bool IsActive { get; }
    public string Description { get; set; }

    public InstallmentTypeDataAttribute(
        short installmentFrequency,
        byte numberOfInstallments,
        bool isActive,
        string description)
    {
        InstallmentFrequency = installmentFrequency;
        NumberOfInstallments = numberOfInstallments;
        IsActive = isActive;
        Description = description;
    }
    public override bool Equals(object obj)
    {
        if (obj == this)
            return true;

        InstallmentTypeDataAttribute other = obj as InstallmentTypeDataAttribute;
        return (other != null)
            && other.InstallmentFrequency == this.InstallmentFrequency
            && other.NumberOfInstallments == this.NumberOfInstallments
            && other.IsActive == this.IsActive;
    }

    public override int GetHashCode() => base.GetHashCode();
    public override bool IsDefaultAttribute() => this.InstallmentFrequency <= 0;
}

[Serializable]
public enum InstallmentTypeEnum
{
    /// <summary>
    /// 1000 - Billed Every 14 Days
    /// </summary>
    [InstallmentTypeData(14, 4, true, "Billed Every 14 Days")]
    BilledEvery14Days = 1000,
}

[Serializable]
public class InstallmentData
{
    public short ProvisionDays { get; set; }
    public decimal InitialInstallmentAmount { get; set; }
    public decimal RenewalInstallmentAmount { get; set; }
    public DateTime InstallmentBillDt { get; set; }
    public DateTime BeginDt { get; set; }
    public DateTime EndDt { get; set; }
}

public static class InstallmentTypeExtensions
{
    public static IEnumerable<InstallmentTypeEnum> GetAllPaymentMethodTypes()
        => Enum.GetValues(typeof(InstallmentTypeEnum)).Cast<InstallmentTypeEnum>();

    public static InstallmentTypeDataAttribute GetInstallmentTypeData(
        this InstallmentTypeEnum installmentType)
    {
        FieldInfo fi = installmentType.GetType().GetField(installmentType.ToString());
        InstallmentTypeDataAttribute[] attributes =
            (InstallmentTypeDataAttribute[])fi.GetCustomAttributes(typeof(InstallmentTypeDataAttribute), false);
        return (attributes != null && attributes.Length > 0)
            ? attributes[0]
            : null;
    }
}


// Test data

[TestFixture]
public class InstallmentsPlanCalculatorTests
{
    [TestCaseSource(nameof(TestCaseDataSet))]
    public void RunTest(TestData testData)
    {
        var planCalculator = new InstallmentsPlanCalculator();
        var calculatedPlan = planCalculator.CalculateInstallmentPlan(
            testData.InstallmentTypeParam,
            testData.OrderDateParam,
            testData.ProductDaysParam,
            testData.SalesPriceParam,
            testData.RenewalPriceParam).ToArray();

        Console.WriteLine(ToDbgString(testData));
        Console.WriteLine(ToDbgString(calculatedPlan));

        calculatedPlan.Should().NotBeNull();
        calculatedPlan.Should().NotBeEmpty();
        
        calculatedPlan.Sum(x => x.ProvisionDays).Should().Be(testData.ProductDaysParam);
        calculatedPlan.Sum(x => x.InitialInstallmentAmount).Should().Be(testData.SalesPriceParam);
        calculatedPlan.Sum(x => x.RenewalInstallmentAmount).Should().Be(testData.RenewalPriceParam);
        
        calculatedPlan.First().BeginDt.Should().Be(testData.OrderDateParam);
        for (int j = 1; j < calculatedPlan.Length; j++)
        {
            calculatedPlan[j].BeginDt.Should().Be(calculatedPlan[j-1].EndDt);
        }
        calculatedPlan.Last().EndDt.Should().Be(testData.OrderEndDt);

        if (testData.ExpectedResult != null)
        {
            calculatedPlan.Should().BeEquivalentTo(testData.ExpectedResult);
        }
    }

    public static IEnumerable<TestCaseData> TestCaseDataSet
        => TestDataSet.Select(testData
            => new TestCaseData(testData).SetName(testData.TestTitle));

    public static IEnumerable<TestData> TestDataSet
    {
        get
        {
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 188.91m,
                RenewalPriceParam = 251.88m,
                ProductDaysParam = 365,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-12-22"),
            };
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 107.93m,
                RenewalPriceParam = 143.91m,
                ProductDaysParam = 365,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-12-22"),
            };
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 53.97m,
                RenewalPriceParam = 107.94m,
                ProductDaysParam = 183,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-06-23"),
            };
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 65.97m,
                RenewalPriceParam = 137.94m,
                ProductDaysParam = 183,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-06-23"),
            };
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 29.99m,
                RenewalPriceParam = 113.97m,
                ProductDaysParam = 92,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-03-24"),
            };
            yield return new TestData
            {
                InstallmentTypeParam = InstallmentTypeEnum.BilledEvery14Days,
                SalesPriceParam = 51.73m,
                RenewalPriceParam = 68.97m,
                ProductDaysParam = 92,
                OrderDateParam = DateTime.Parse("2020-12-22"),
                OrderEndDt = DateTime.Parse("2021-03-24"),
            };
    

        }
	}

    public static string ToDbgString(IEnumerable<InstallmentData> installmentPlan)
    {
        if (installmentPlan == null)
            return string.Empty;

        const string l = "------";

        StringBuilder sb = new StringBuilder();

        sb.AppendFormat("| {0,10} | {1,10} | {2,10} | {3,12} | {4,10} | {5,10} |", "ProvDays", "InstAmnt", "RenewAmnt", "BillDt", "BeginDt", "EndDt");
        sb.AppendLine();
        sb.AppendFormat("| {0,10} | {1,10} | {2,10} | {3,12} | {4,10} | {5,10} |", l, l, l, l, l, l);
        sb.AppendLine();

        foreach (var x in installmentPlan)
        {
            sb.AppendFormat("| {0,10} | {1,10:F2} | {2,10:F2} | {3,12:yy-MM-dd} | {4,10:yy-MM-dd} | {5,10:yy-MM-dd} |", x.ProvisionDays, x.InitialInstallmentAmount, x.RenewalInstallmentAmount, x.InstallmentBillDt, x.BeginDt, x.EndDt);
            sb.AppendLine();
        }

        sb.AppendLine();
        return sb.ToString();
    }

    public static string ToDbgString(TestData testData)
    {
        const string l = "-------";
        StringBuilder sb = new StringBuilder();
        byte numberOfInstallments = testData.InstallmentTypeParam.GetInstallmentTypeData().NumberOfInstallments;

        sb.Append(testData.TestTitle);
        sb.AppendLine();
        sb.AppendLine();
        sb.AppendFormat("| {0,10} | {1,10} | {2,10} | {3,12} | {4,10} | {5,12} | {6,10} |", "Order date", "End date", "Prod.days", "Sales price", "1/4 Avg", "Renew price", "1/4 Avg");
        sb.AppendLine();
        sb.AppendFormat("| {0,10} | {1,10} | {2,10} | {3,12} | {4,10} | {5,12} | {6,10} |", l, l, l, l, l, l, l);
        sb.AppendLine();
        sb.AppendFormat("| {0,10:yy-MM-dd} | {1,10:yy-MM-dd} | {2,10} | {3,12:F2} | {4,10:F4} | {5,12:F2} | {6,10:F4} |", testData.OrderDateParam, testData.OrderEndDt, testData.ProductDaysParam,
            testData.SalesPriceParam,
            testData.SalesPriceParam / numberOfInstallments,
            testData.RenewalPriceParam,
            testData.RenewalPriceParam / numberOfInstallments);
        sb.AppendLine();
        sb.AppendLine();

        return sb.ToString();
    }
    
    public class TestData
    {
        public string TestTitle => $"{this.InstallmentTypeParam}: {ProductDaysParam} days ({OrderDateParam.ToString("yy-MM-dd")}..{OrderEndDt.ToString("yy-MM-dd}")}), salesPrice={SalesPriceParam}, renewalPrice={RenewalPriceParam}";
        public InstallmentTypeEnum InstallmentTypeParam { get; set; }
        public DateTime OrderDateParam { get; set; }
        public short ProductDaysParam { get; set; }
        public decimal SalesPriceParam { get; set; }
        public decimal RenewalPriceParam { get; set; }
        public DateTime OrderEndDt { get; set; }
        public IEnumerable<InstallmentData> ExpectedResult { get; set; }
    }
}
