<Query Kind="Program" />

void Main()
{
    string path = @"D:\Projects\Match\Billing\BillingService\Corp.Billing.Data\DataAccess";
    if (!Directory.Exists(path))
    {
        Console.WriteLine($"path does not exists: {path}");
    }

    List<string[]> repos = new List<string[]>
    {
        new[] {"BillingServiceRepository", "[BillingService]", "WriteBillingService", "User ID=Jup1t3r;Password=7k*R3?lk;Initial Catalog=BillingService;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"BillingCodeRepository", "[BillingCode]", "WriteBillingCode", "User ID=Jup1t3r;Password=7k*R3?lk;Initial Catalog=BillingCode;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"CareBillingCodeRepository", "[BillingCode]", "WriteCareBillingCode", "User ID=CSAAPP;Password=k33p`3m;Initial Catalog=BillingCode;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"CareMatchCodeRepository", "[CSAMatchCode]", "WriteCareMatchCode", "User ID=CSAAPP;Password=k33p`3m;Initial Catalog=CSAMatchCode;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"LoadBalRepository", "[LoadBal5]", "LoadBal5", "User ID=3acchu$;Password=J8$df.q[H&amp;;Initial Catalog=LoadBal5;Data Source=DAL16;APP=BillingService;"},
        new[] {"Match5Repository", "[Match5]", "Match5", "User ID=Jup1t3r;Password=7k*R3?lk;Initial Catalog=Match5;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"ProfileReadRepository", "[ProfileReadCode]", "ProfileReadCode", "User ID=3acchu$;Password=J8$df.q[H&amp;;Initial Catalog=ProfileReadCode;Data Source=DAL16;APP=BillingService;"},
        new[] {"ProfileWriteRepository", "[ProfileCode]", "ProfileWriteCode", "User ID=Jup1t3r;Password=7k*R3?lk;Initial Catalog=ProfileCode;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
        new[] {"WriteOSDBRepository", "[OSDB]", "WriteOSDB", "User ID=Jup1t3r;Password=7k*R3?lk;Initial Catalog=OSDB;Data Source=tcp:MASW,1433;APP=BillingService;MultiSubnetFailover=True"},
    };

    Console.WriteLine($"SP Name~File Name~Base Class~Connection String Name~Connection String");
    
    foreach (var repo in repos)
    {
        PrintStoredProceduresForRepo(path, repo[0], repo[1], repo[2], repo[3]);
    }
}

private static void PrintStoredProceduresForRepo(string path, string baseClass, string defaultSchema, string connectionStringName, string connectionString)
{
    var files = new List<string>();
    GetFileNames(path, ".cs", baseClass, files);

    foreach (var data in GetStoredProcedures(files))
    {
        var fileName = data.Item1.Replace(path, string.Empty);
        var spName = data.Item2;
        if (spName.Count(x => x == '.') == 1)
        {
            spName = $"{defaultSchema}.{spName}";
            spName = spName.Replace(".dbo.", ".[dbo].");
            if (spName.Contains("].[dbo].") && spName.Contains("].[dbo].[") == false)
            {
                spName = spName.Replace("].[dbo].", "].[dbo].[");
            }
            if (!spName.EndsWith("]"))
            {
                spName = spName + "]";
            }
        }

        // if (string.IsNullOrWhiteSpace(spName))
        Console.WriteLine($"{spName}~{fileName}~{baseClass}~{connectionStringName}~{connectionString}");
    }
}

private static IEnumerable<Tuple<string, string>> GetStoredProcedures(List<string> files)
{
    foreach (var file in files)
    {
        string spName = null;
        string[] lines = File.ReadAllLines(file);
        foreach (var line in lines)
        {
            if (line.Contains("]\"")
                || line.Contains("\"dbo."))
            {
                var lineParts = line.Split(new string[] { "\"", }, StringSplitOptions.RemoveEmptyEntries);
                spName = (lineParts.Length > 1) ? lineParts[1] : line;
            }
        }
        
        yield return new Tuple<string, string>(file, spName);
    }
}

private static void GetFileNames(string path, string ext, string contains, List<string> result)
{
    var subDirs = Directory.GetDirectories(path);
    foreach (var subDir in subDirs)
    {
        if (!subDir.EndsWith("BaseRepositories"))
        {
            GetFileNames(subDir, ext, contains, result);
        }
    }

    var files = Directory.GetFiles(path);
    foreach (var file in files)
    {
        if (!file.EndsWith(ext))
        {
            continue;
        }

        string fileText = File.ReadAllText(file);

        if (!fileText.Contains(contains))
        {
            continue;
        }
        
        result.Add(file);
    }
}