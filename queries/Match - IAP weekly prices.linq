<Query Kind="Program" />

void Main()
{
	Console.WriteLine($@"

<!-- IAP_NonRenewable : Weekly Conditions -->

");

	Console.WriteLine($@"
<add name=""IAP_NonRenewable_Weekly"" continue-on-success=""true"">
  <conditions>
    <add name=""IAP_NonRenewable_Weekly: **START**  "" type=""BooleanCondition"" value=""true"" />
    <add name=""IAP_NonRenewable_Weekly: IAP CatalogType    "" type=""CatalogTypeCondition"" value=""InApp"" />
    <add name=""IAP_NonRenewable_Weekly: IAP CatalogSource  "" type=""CatalogSourceCondition"" value=""Apple"" />
    <add name=""IAP_NonRenewable_Weekly:            "" type=""RulesEvaluateCondition"" value=""~/Resources/Tests/IAP_NonRenewable_Weekly.xml"" />
  </conditions>
  <on-success-results>"
	);

	for (int i = 1; i <= 26; i++)
	{
		int d1 = 7 * i;
		int d2 = 7 * (i + 1) - 1;
		int gt = d1 - 1;
		int lt = d2 + 1;
		// Console.WriteLine($"{i}: {d1}-{d2} : {gt}-{lt}");
		Console.WriteLine($@"
    <results>
      <conditions>
        <add name=""IAP_NonRenewable_{i}w: days {d1}-{d2}"" type=""BaseDaysRemainingCondition"" gt=""{gt}"" lt=""{lt}"" />
        <add name=""IAP_NonRenewable_{i}w"" type=""SetMemoCondition"" value=""1""/>
      </conditions>
    </results>
");
	}

	for (int i = 6; i <= 12; i++)
	{
		int d1 = (i == 6) ? 189 : i * 30;
		int d2 = 30 * (i + 1) - 1;
		
		int gt = d1 - 1;
		int lt = d2 + 1;

		Console.WriteLine($@"
    <results>
      <conditions>
        <add name=""IAP_NonRenewable_{i}m: days {d1}-{d2}"" type=""BaseDaysRemainingCondition"" gt=""{gt}"" lt=""{lt}"" />
        <add name=""IAP_NonRenewable_{i}m"" type=""SetMemoCondition"" value=""1""/>
      </conditions>
    </results>
");
	}

	Console.WriteLine(
$@"
  </on-success-results>
</add>"
	);

	// -----

	var rffs = GetRFFOverrides();
	var pms = GetPMOverrides();

	Console.WriteLine($@"

<!-- IAP_NonRenewable : Overrides -->

");

	for (int i = 1; i <= 26; i++)
	{
		var rff = rffs[i - 1];
		var pm = pms[i - 1];

		Console.WriteLine($@"
<add name=""IAP_NonRenewable_{i}w_rff"" continue-on-success=""true"">
  <conditions>
    <add name=""IAP_NonRenewable_{i}w"" type=""HasMemoCondition"" value=""1"" />
    <add name=""IAP_NonRenewable_{i}w_rff"" type=""HasPrvFeatureCondition"" value=""28"" not=""true""/>
  </conditions>
  <on-success-results>
    <add name=""RateCardOverrideKey"" value=""{rff.Item1}"" append=""true"" />
  </on-success-results>
</add>
<add name=""IAP_NonRenewable_{i}w_pm"" continue-on-success=""true"">
  <conditions>
    <add name=""IAP_NonRenewable_{i}w"" type=""HasMemoCondition"" value=""1"" />
    <add name=""IAP_NonRenewable_{i}w_pm"" type=""HasPrvFeatureCondition"" value=""29"" not=""true""/>
  </conditions>
  <on-success-results>
    <add name=""RateCardOverrideKey"" value=""{pm.Item1}"" append=""true"" />
  </on-success-results>
</add>
");
	}
}

public static IList<Tuple<int, string>> GetRFFOverrides()
{
	var res = new List<Tuple<int, string>>();
	
	res.Add(Tuple.Create(990098, "com.match.match.com.unit.RFFweek1" ));
	res.Add(Tuple.Create(990099, "com.match.match.com.unit.RFFweek2" ));
	res.Add(Tuple.Create(990100, "com.match.match.com.unit.RFFweek3" ));
	res.Add(Tuple.Create(990101, "com.match.match.com.unit.RFFweek4" ));
	res.Add(Tuple.Create(990102, "com.match.match.com.unit.RFFweek5" ));
	res.Add(Tuple.Create(990103, "com.match.match.com.unit.RFFweek6" ));
	res.Add(Tuple.Create(990104, "com.match.match.com.unit.RFFweek7" ));
	res.Add(Tuple.Create(990105, "com.match.match.com.unit.RFFweek8" ));
	res.Add(Tuple.Create(990106, "com.match.match.com.unit.RFFweek9" ));
	res.Add(Tuple.Create(990107, "com.match.match.com.unit.RFFweek10"));
	res.Add(Tuple.Create(990108, "com.match.match.com.unit.RFFweek11"));
	res.Add(Tuple.Create(990109, "com.match.match.com.unit.RFFweek12"));
	res.Add(Tuple.Create(990110, "com.match.match.com.unit.RFFweek13"));
	res.Add(Tuple.Create(990111, "com.match.match.com.unit.RFFweek14"));
	res.Add(Tuple.Create(990112, "com.match.match.com.unit.RFFweek15"));
	res.Add(Tuple.Create(990113, "com.match.match.com.unit.RFFweek16"));
	res.Add(Tuple.Create(990114, "com.match.match.com.unit.RFFweek17"));
	res.Add(Tuple.Create(990115, "com.match.match.com.unit.RFFweek18"));
	res.Add(Tuple.Create(990116, "com.match.match.com.unit.RFFweek19"));
	res.Add(Tuple.Create(990117, "com.match.match.com.unit.RFFweek20"));
	res.Add(Tuple.Create(990118, "com.match.match.com.unit.RFFweek21"));
	res.Add(Tuple.Create(990119, "com.match.match.com.unit.RFFweek22"));
	res.Add(Tuple.Create(990120, "com.match.match.com.unit.RFFweek23"));
	res.Add(Tuple.Create(990121, "com.match.match.com.unit.RFFweek24"));
	res.Add(Tuple.Create(990122, "com.match.match.com.unit.RFFweek25"));
	res.Add(Tuple.Create(990123, "com.match.match.com.unit.RFFweek26"));

	return res;
}

public static IList<Tuple<int, string>> GetPMOverrides()
{
	var res = new List<Tuple<int, string>>();

	res.Add(Tuple.Create(990124, "com.match.match.com.unit.PrivateModeweek1" ));
	res.Add(Tuple.Create(990125, "com.match.match.com.unit.PrivateModeweek2" ));
	res.Add(Tuple.Create(990126, "com.match.match.com.unit.PrivateModeweek3" ));
	res.Add(Tuple.Create(990127, "com.match.match.com.unit.PrivateModeweek4" ));
	res.Add(Tuple.Create(990128, "com.match.match.com.unit.PrivateModeweek5" ));
	res.Add(Tuple.Create(990129, "com.match.match.com.unit.PrivateModeweek6" ));
	res.Add(Tuple.Create(990130, "com.match.match.com.unit.PrivateModeweek7" ));
	res.Add(Tuple.Create(990131, "com.match.match.com.unit.PrivateModeweek8" ));
	res.Add(Tuple.Create(990132, "com.match.match.com.unit.PrivateModeweek9" ));
	res.Add(Tuple.Create(990133, "com.match.match.com.unit.PrivateModeweek10"));
	res.Add(Tuple.Create(990134, "com.match.match.com.unit.PrivateModeweek11"));
	res.Add(Tuple.Create(990135, "com.match.match.com.unit.PrivateModeweek12"));
	res.Add(Tuple.Create(990136, "com.match.match.com.unit.PrivateModeweek13"));
	res.Add(Tuple.Create(990137, "com.match.match.com.unit.PrivateModeweek14"));
	res.Add(Tuple.Create(990138, "com.match.match.com.unit.PrivateModeweek15"));
	res.Add(Tuple.Create(990139, "com.match.match.com.unit.PrivateModeweek16"));
	res.Add(Tuple.Create(990140, "com.match.match.com.unit.PrivateModeweek17"));
	res.Add(Tuple.Create(990141, "com.match.match.com.unit.PrivateModeweek18"));
	res.Add(Tuple.Create(990142, "com.match.match.com.unit.PrivateModeweek19"));
	res.Add(Tuple.Create(990143, "com.match.match.com.unit.PrivateModeweek20"));
	res.Add(Tuple.Create(990144, "com.match.match.com.unit.PrivateModeweek21"));
	res.Add(Tuple.Create(990145, "com.match.match.com.unit.PrivateModeweek22"));
	res.Add(Tuple.Create(990146, "com.match.match.com.unit.PrivateModeweek23"));
	res.Add(Tuple.Create(990147, "com.match.match.com.unit.PrivateModeweek24"));
	res.Add(Tuple.Create(990148, "com.match.match.com.unit.PrivateModeweek25"));
	res.Add(Tuple.Create(990149, "com.match.match.com.unit.PrivateModeweek26"));

	return res;
}

