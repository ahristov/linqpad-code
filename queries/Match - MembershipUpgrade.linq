<Query Kind="Program">
  <Namespace>System.Security.Cryptography</Namespace>
  <Namespace>System.Xml.Serialization</Namespace>
</Query>

void Main()
{
	var data = GetExampleData();
	Console.WriteLine(SerializeToXml(data));
}


#region Data

private static MembershipUpgradeConfiguration GetExampleData()
{
	return new MembershipUpgradeConfiguration
	{
		MembershipUpgradeRules = new MembershipUpgradeRules
		{
			Rules = new List<MembershipUpgradeRule>
			{
				new MembershipUpgradeRule
				{
					Order = 1010,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.Default,
					CatalogSource = CatalogSource.Default,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 1,
					CurrentPackageIsAppleIAP = false,
					UserIdHashTest = "1581125832|01234567|11",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 3, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 1020,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.Default,
					CatalogSource = CatalogSource.Default,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 1,
					CurrentPackageIsAppleIAP = false,
					UserIdHashTest = "1581125832|89ABCDEF|11",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 6, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 1030,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.Default,
					CatalogSource = CatalogSource.Default,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 3,
					CurrentPackageIsAppleIAP = false,
					UserIdHashTest = "",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 3, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 1040,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.Default,
					CatalogSource = CatalogSource.Default,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 6,
					CurrentPackageIsAppleIAP = false,
					UserIdHashTest = "",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 6, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 1050,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.Default,
					CatalogSource = CatalogSource.Default,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 12,
					CurrentPackageIsAppleIAP = false,
					UserIdHashTest = "",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 12, }
						}
					}
				},


				new MembershipUpgradeRule
				{
					Order = 2010,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.InApp,
					CatalogSource = CatalogSource.Apple,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 1,
					CurrentPackageIsAppleIAP = true,
					UserIdHashTest = "1581125832|01234567|11",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 3, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 2020,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.InApp,
					CatalogSource = CatalogSource.Apple,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 1,
					CurrentPackageIsAppleIAP = true,
					UserIdHashTest = "1581125832|89ABCDEF|11",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 6, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 2030,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.InApp,
					CatalogSource = CatalogSource.Apple,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 3,
					CurrentPackageIsAppleIAP = true,
					UserIdHashTest = "",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 3, }
						}
					}
				},
				new MembershipUpgradeRule
				{
					Order = 2040,
					SiteCodes = "1",
					CountryCodes = "1,2",
					CatalogType = CatalogType.InApp,
					CatalogSource = CatalogSource.Apple,
					CurrentPackageType = SubscriptionPackageTypeEnum.StandardPackage,
					CurrentPackageMonths = 6,
					CurrentPackageIsAppleIAP = true,
					UserIdHashTest = "",
					Disabled = false,
					StartDateTime = null,
					EndDateTime = null,
					MembershipUpgradeResults = new MembershipUpgradeResults
					{
						Results = new List<MembershipUpgradePackage>
						{
							new MembershipUpgradePackage { UpgradeBundleType = BundleType.Premium, UpgradePackageMonths = 6, }
						}
					}
				}
			},
		}
	};
}

public static string SerializeToXml(object obj)
{
	if (obj == null)
		return null;

	XmlSerializer serializer = new XmlSerializer(obj.GetType());
	StringBuilder sb = new StringBuilder();
	using (TextWriter w = new StringWriter(sb))
	{
		serializer.Serialize(w, obj);
	}
	return sb.ToString();
}

#endregion Data


#region Corp.Billing.Core.MembershipUpgrade.Services

	public class MembershipUpgradePathRequest
	{
		public int UserId { get; set; }
		public int SiteCode { get; set; }
		public int CountryCode { get; set; }
		public CatalogType CatalogType { get; set; }
		public CatalogSource CatalogSource { get; set; }
		public SubscriptionPackageTypeEnum CurrentPackageType { get; set; }
		public byte CurrentPackageMonths { get; set; }
		public bool IsCurrentPackageAppleIAP { get; set; }
	}

	public class MembershipUpgradePathResponse
	{
		public IList<UpgradePackage> UpgradePackages { get; set; }
		
		public MembershipUpgradePathResponse()
		{
			UpgradePackages = new List<UpgradePackage>();
		}

		public class UpgradePackage
		{
			public BundleType PackageBundleType { get; set; }
			public int PackageMonths { get; set; }
		}
	}

	public interface IMembershipUpgradePath
	{ 
		MembershipUpgradePathResponse FindUpdgradePaths(MembershipUpgradePathRequest request, MembershipUpgradeConfiguration configuration);
	}

	public class MembershipUpgradePath : IMembershipUpgradePath
	{
		public MembershipUpgradePathResponse FindUpdgradePaths(MembershipUpgradePathRequest request, MembershipUpgradeConfiguration configuration)
		{
			throw new NotImplementedException();
		}
	}


#endregion Corp.Billing.Core.MembershipUpgrade.Services


#region Corp.Billing.Core.MembershipUpgrade.Configuration

	public class MembershipUpgradeRule
	{
		[XmlAttribute(AttributeName = "order")]
		public int Order { get; set; }
		
		// Evaliuate conditions: Compare with current user status
		
		[XmlAttribute(AttributeName = "site-codes")]
		public string SiteCodes { get; set; }
		[XmlIgnore]
		public int[] SiteCodesArray => ("" + SiteCodes).Split(',').Select(x => int.TryParse(x, out int i) ? i : 0).Where(x => x > 0).ToArray();

		[XmlAttribute(AttributeName = "country-codes")]
		public string CountryCodes { get; set; }
		[XmlIgnore]
		public int[] CountryCodesArray => ("" + CountryCodes).Split(',').Select(x => int.TryParse(x, out int i) ? i : 0).Where(x => x > 0).ToArray();

		[XmlAttribute(AttributeName = "catalog-type")]
		public string CatalogTypeAsString
		{
			get { return CatalogType.HasValue ? CatalogType.ToString() : null; }
			set { CatalogType = !string.IsNullOrEmpty(value) ? (CatalogType)Enum.Parse(typeof(CatalogType), value) : (CatalogType?)null; }
		}
		[XmlIgnore]
		public CatalogType? CatalogType { get; set; }

		[XmlAttribute(AttributeName = "catalog-source")]
		public string CatalogSourceAsString
		{
			get { return CatalogSource.HasValue ? CatalogSource.ToString() : null; }
			set { CatalogSource = !string.IsNullOrEmpty(value) ? (CatalogSource)Enum.Parse(typeof(CatalogSource), value) : (CatalogSource?)null; }
		}
		[XmlIgnore]
		public CatalogSource? CatalogSource { get; set; }

		[XmlAttribute(AttributeName = "current-package-type")]
		public string CurrentPackageTypeAsString
		{
			get { return CurrentPackageType.ToString(); }
			set { CurrentPackageType = (SubscriptionPackageTypeEnum)Enum.Parse(typeof(SubscriptionPackageTypeEnum), value); }
		}
		[XmlIgnore]
		public SubscriptionPackageTypeEnum CurrentPackageType { get; set; }

		[XmlAttribute(AttributeName = "current-package-months")]
		public int CurrentPackageMonths { get; set; }

		[XmlIgnore]
		public bool? CurrentPackageIsAppleIAP { get; set; }
		[XmlAttribute(AttributeName = "current-package-is-iap")]
		public string CurrentPackageIsAppleIAPAsString
		{
			get { return CurrentPackageIsAppleIAP.HasValue ? CurrentPackageIsAppleIAP.ToString() : null; }
			set { CurrentPackageIsAppleIAP = !string.IsNullOrWhiteSpace(value) ? bool.Parse(value) : default(bool?); }
		}
		[XmlIgnore]
		public bool CurrentPackageIsAppleIAPSpecified { get => CurrentPackageIsAppleIAP.HasValue; }

		// AB Testing: User ID hash
		
		[XmlAttribute(AttributeName = "userid-hash-test")]
		public string UserIdHashTest { get; set; }
		[XmlIgnore]
		public bool UserIdHashTestSpecified { get => !String.IsNullOrWhiteSpace((""+UserIdHashTest).Trim()); }

		// Flow control conditions: Is it active, when it starts or finishes

		[XmlAttribute(AttributeName = "disabled")]
		public bool Disabled { get; set; }
		[XmlIgnore]
		public bool DisabledSpecified { get => Disabled; }


		[XmlAttribute(AttributeName = "start-date-time")]
		public string StartDateTimeAsText
		{
			get { return StartDateTime.HasValue ? StartDateTime.ToString() : null; }
			set { StartDateTime = !string.IsNullOrEmpty(value) ? DateTime.Parse(value) : default(DateTime?); }
		}
		[XmlIgnore]
		public DateTime? StartDateTime { get; set; }

		[XmlAttribute(AttributeName = "end-date-time")]
		public string EndDateTimeAsText
		{
			get { return EndDateTime.HasValue ? EndDateTime.ToString() : null; }
			set { EndDateTime = !string.IsNullOrEmpty(value) ? DateTime.Parse(value) : default(DateTime?); }
		}
		[XmlIgnore]
		public DateTime? EndDateTime { get; set; }

		[XmlElement(ElementName = "membership-upgrade-results")]
		public MembershipUpgradeResults MembershipUpgradeResults { get; set; }

	}

	public class MembershipUpgradeRules
	{
		[XmlElement(ElementName = "membership-upgrade-rule")]
		public List<MembershipUpgradeRule> Rules { get; set; }

		public MembershipUpgradeRules()
		{
			Rules = new List<MembershipUpgradeRule>();
		}
	}

	[XmlRoot(ElementName = "membership-upgrade-configuration")]
	public class MembershipUpgradeConfiguration
	{
		[XmlElement(ElementName = "membership-upgrade-rules")]
		public MembershipUpgradeRules MembershipUpgradeRules { get; set; }

		public MembershipUpgradeConfiguration()
		{
			MembershipUpgradeRules = new MembershipUpgradeRules();
		}
	}


	public class MembershipUpgradePackage
	{
		[XmlAttribute(AttributeName = "upgrade-bundle-type")]
		public BundleType UpgradeBundleType { get; set; }

		[XmlAttribute(AttributeName = "upgrade-package-months")]
		public int UpgradePackageMonths { get; set; }
	}

	public class MembershipUpgradeResults
	{
		[XmlElement(ElementName = "membership-upgrade-package")]
		public List<MembershipUpgradePackage> Results { get; set; }

		public MembershipUpgradeResults()
		{
			Results = new List<MembershipUpgradePackage>();
		}
	}


#endregion Corp.Billing.Core.MembershipUpgrade.Configuration










#region Corp.Billing.Shared.Domain.RateCard

	public enum BundleType : byte
	{
		None = 0,
		Premium = 1,
		Upsell = 2,
		AddOn = 3,
		Elite = 4,
	}

#endregion Corp.Billing.Shared.Domain.RateCard

#region Corp.Billing.Shared.Domain.RateCardV3.Data

	public enum CatalogType : int
	{
		Default = 0,
		InApp = 1,
	}
	public enum CatalogSource : int
	{
		Default = 0,
		Apple = 1,
	}

#endregion Corp.Billing.Shared.Domain.RateCardV3.Data

#region Corp.Billing.Shared.Domain.Subscription

	public enum SubscriptionPackageTypeEnum : byte
	{
		NonSubscriber = 0,
		StandardPackage = 1,
		PremiumPackage = 2,
		ElitePackage = 3,
	}
	
#endregion Corp.Billing.Shared.Domain.Subscription


#region Corp.Billing.Core.Facilities

	public class UserIdHashEvaluator
	{
		private string _matchCharacters;
		private int _position;

		public void Init(string criteria)
		{
			string[] pieces = criteria.Split('|');

			if (pieces.Length != 2)
				throw new ArgumentException("Initialization parameters in incorrect format");

			_matchCharacters = pieces[0].ToLower();
			int.TryParse(pieces[1], out _position);
		}

		public bool Evaluate(int userid, string salt)
		{
			string hash = calcuateUserIDTestHash(userid, salt).ToLower();
			return _matchCharacters.IndexOf(hash[_position - 1]) >= 0;
		}


		//DO NOT MODIFY, Mirrors hash in MCoreAssembly
		private string calcuateUserIDTestHash(int userId, string testName)
		{
			string plainText = userId + testName;
			SHA1 cryptoProvider = new SHA1CryptoServiceProvider();
			byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			byte[] cipherTextBytes = cryptoProvider.ComputeHash(plainTextBytes);
			StringBuilder bufferBuilder = new StringBuilder("0x", 60);
			foreach (byte currentByte in cipherTextBytes)
			{
				bufferBuilder.Append(currentByte.ToString("x2"));
			}
			return bufferBuilder.ToString();
		}


	}
	
#endregion Corp.Billing.Core.Facilities