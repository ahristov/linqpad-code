<Query Kind="Program" />

void Main()
{
	foreach (ProductFeatureType pft in ProductFeatureTools.GetPowerUpFeatureTypes())
	{
		Console.WriteLine($"PowerUp: {pft}={(byte)pft}");
	}
}

// Define other methods and classes here


[AttributeUsage(AttributeTargets.All)]
public class CanSaleAsPowerUpAttribute : Attribute
{

    public override bool Equals(object obj) => obj == this || obj is CanSaleAsPowerUpAttribute;
    public override int GetHashCode()=> base.GetHashCode();
    public override bool IsDefaultAttribute() => true;

}

public enum ProductFeatureType : byte
{
    Undefined = 0,
    EmailCommunication = 1,
    ActiveProfile = 2,
    HighlightedProfile = 3,
    DateCoach = 4,
    FirstImpressions = 5,
    Voice = 6,
    PayForResponse = 7,
    MindFindBind = 8,
[CanSaleAsPowerUp]
    EmailReadNotification = 9,
    MindFindBindSavings = 10,
    PlatinumServicesSavings = 11,
[CanSaleAsPowerUp]
    MatchPhone = 12,
    MatchMobile = 13,
    SugarDaddyEmailToken = 14,
    RemoteApplicationEmailToken = 15,
    ProfileConsultingStandAloneProduct = 16,
    ProfileConsultingSubscriptionRequired = 17,
    Platinum = 18,
    PlatinumPackageSavings = 19,
    ChemistrySubscription = 20,
    MatchEventMemberTickets = 21,
    MatchEventGuestTicketsMale = 22,
    MatchEventGuestTicketsFemale = 23,
    ActivationFee = 24,
    PremiumBundleSavings = 25,
    TopSpot = 26,
    Stealth = 27,
[CanSaleAsPowerUp]	
    ReplyForFree = 28,
[CanSaleAsPowerUp]	
    PrivateMode = 29,
    MatchMe = 30,
    LoveToken = 31,
    SpeedVerify = 32,
    Intuition = 33,
    Invisibility = 34,
    Magnetism = 35,
    SuperStrength = 36,
    MatchIQ = 37,
    MatchVerification = 38,
    SuperLikes=39,
}

public static class ProductFeatureTools
{
    public static bool CanSaleAsPowerUp(this ProductFeatureType productFeature)
    {
        FieldInfo fi = productFeature.GetType().GetField(productFeature.ToString());
        CanSaleAsPowerUpAttribute[] attributes =
            (CanSaleAsPowerUpAttribute[])fi.GetCustomAttributes(typeof(CanSaleAsPowerUpAttribute), false);
        return attributes?.Length > 0;
    }
	
	public static IEnumerable<ProductFeatureType> GetAllProductFeatureTypes() => System.Enum.GetValues(typeof(ProductFeatureType)).Cast<ProductFeatureType>();
    public static IEnumerable<ProductFeatureType> GetPowerUpFeatureTypes(this IEnumerable<ProductFeatureType> pfts) => pfts.Where(pft => pft.CanSaleAsPowerUp());
    public static IEnumerable<ProductFeatureType> GetPowerUpFeatureTypes() => GetAllProductFeatureTypes().GetPowerUpFeatureTypes();
}
